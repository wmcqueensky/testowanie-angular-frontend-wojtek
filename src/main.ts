import 'hammerjs';
import './polyfills';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import './assets/plugins/ripple-effect/ripple-effect';

platformBrowserDynamic().bootstrapModule(AppModule);
