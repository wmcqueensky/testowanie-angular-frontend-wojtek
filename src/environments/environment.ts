import { IEnvironment } from "./interfaces";


export const environment: IEnvironment = {
  production: false,
  baseUrl: 'https://testangular.glpsoftware.pl',
}
