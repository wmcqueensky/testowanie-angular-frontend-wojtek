import { IEnvironment } from "./interfaces";


export const environment: IEnvironment = {
  production: true,
  baseUrl: 'https://testangular.glpsoftware.pl',
}
