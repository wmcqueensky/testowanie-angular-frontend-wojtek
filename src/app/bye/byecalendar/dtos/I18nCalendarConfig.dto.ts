import { DAYS_OF_WEEK_NAME, DAYS_OF_WEEK_SHORT_NAME, MONTHES_NAME, MONTHES_SHORT_NAME } from "../models/constants";

export default class I18nCalendarConfigDTO {
  monthes?: string[];
  monthesShort?: string[];
  daysOfWeek?: string[];
  daysOfWeekShort?: string[];
  today?: string;
  month?: string;
  week?: string;
  day?: string;
  viewWeek?: string;
  viewDay?: string;
  add?: string;
  eventEditor?: string;
  title?: string;
  titleWarning?: string;
  color?: string;
  radioDate?: string;
  radioTime?: string;
  radioDaterange?: string;
  radioTimerange?: string;
  date?: string;
  time?: string;
  dateFrom?: string;
  dateTo?: string;
  datetimeFrom?: string;
  datetimeTo?: string;
  save?: string;
  edit?: string;
  remove?: string;
  removeConfirmHeader?: string;
  removeConfirmRemove?: string;
  removeConfirmReset?: string;

  constructor(config?: I18nCalendarConfigDTO) {
    if (!config) config = {};

    this.monthes = config.monthes || MONTHES_NAME;
    this.monthesShort = config.monthesShort || MONTHES_SHORT_NAME;
    this.daysOfWeek = config.daysOfWeek || DAYS_OF_WEEK_NAME;
    this.daysOfWeekShort = config.daysOfWeekShort || DAYS_OF_WEEK_SHORT_NAME;
    this.today = config.today || 'BYECALENDAR_TODAY';
    this.month = config.month || 'BYECALENDAR_MONTH';
    this.week = config.week || 'BYECALENDAR_WEEK';
    this.day = config.day || 'BYECALENDAR_DAY';
    this.viewWeek = config.viewWeek || 'BYECALENDAR_VIEW_WEEK';
    this.viewDay = config.viewDay || 'BYECALENDAR_VIEW_DAY';
    this.add = config.add || 'BYECALENDAR_ADD';
    this.eventEditor = config.eventEditor || 'BYECALENDAR_EVENT_EDITOR';
    this.title = config.title || 'BYECALENDAR_TITLE';
    this.titleWarning = config.titleWarning || 'BYECALENDAR_TITLE_WARNING';
    this.color = config.color || 'BYECALENDAR_COLOR';
    this.radioDate = config.radioDate || 'BYECALENDAR_RADIO_DATE';
    this.radioTime = config.radioTime || 'BYECALENDAR_RADIO_TIME';
    this.radioDaterange = config.radioDaterange || 'BYECALENDAR_RADIO_DATERANGE';
    this.radioTimerange = config.radioTimerange || 'BYECALENDAR_RADIO_TIMERANGE';
    this.date = config.date || 'BYECALENDAR_DATE';
    this.time = config.time || 'BYECALENDAR_TIME';
    this.dateFrom = config.dateFrom || 'BYECALENDAR_DATE_FROM';
    this.dateTo = config.dateTo || 'BYECALENDAR_DATE_TO';
    this.datetimeFrom = config.datetimeFrom || 'BYECALENDAR_DATETIME_FROM';
    this.datetimeTo = config.datetimeTo || 'BYECALENDAR_DATETIME_TO';
    this.save = config.save || 'BYECALENDAR_SAVE';
    this.edit = config.edit || 'BYECALENDAR_EDIT';
    this.remove = config.remove || 'BYECALENDAR_REMOVE';
    this.removeConfirmHeader = config.removeConfirmHeader || 'BYECALENDAR_REMOVE_CONFIRM_HEADER';
    this.removeConfirmRemove = config.removeConfirmRemove || 'BYECALENDAR_REMOVE_CONFIRM_REMOVE';
    this.removeConfirmReset = config.removeConfirmReset || 'BYECALENDAR_REMOVE_CONFIRM_RESET';
  }
}