import { AdditionalUtilsService } from './services/additional-utils.service';
import { MainUtilsService } from './services/main-utils.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ByeCalendarComponent } from './byecalendar.component';
import { MonthViewComponent } from './components/month-view/month-view.component';
import { WeekViewComponent } from './components/week-view/week-view.component';
import { DayViewComponent } from './components/day-view/day-view.component';
import { TimerangeFromIndexPipe } from './pipes/timerange-from-index.pipe';
import { WeekCalendarHeaderPipe } from './pipes/week-calendar-header.pipe';
import { TimerangeFromHourPipe } from './pipes/timerange-from-hour.pipe';
import { MonthEventsFilterPipe } from './pipes/month-events-filter.pipe';
import { WeekEventsFilterPipe } from './pipes/week-events-filter.pipe';
import { DayEventsFilterPipe } from './pipes/day-events-filter.pipe';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { GLPDateComponent } from './components/date/date.component';
import { GLPDatetimeComponent } from './components/datetime/datetime.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { ArrayLimitPipe } from './pipes/array-limit.pipe';



@NgModule({
  declarations: [
    ByeCalendarComponent,
    MonthViewComponent,
    WeekViewComponent,
    DayViewComponent,
    TimerangeFromIndexPipe,
    WeekCalendarHeaderPipe,
    TimerangeFromHourPipe,
    MonthEventsFilterPipe,
    WeekEventsFilterPipe,
    DayEventsFilterPipe,
    ArrayLimitPipe,
    GLPDateComponent,
    GLPDatetimeComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ColorPickerModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      },
      defaultLanguage: 'pl'
    }),
  ],
  exports: [
    ByeCalendarComponent
  ],
  providers: [
    DayEventsFilterPipe,
    MainUtilsService,
    AdditionalUtilsService,
    DatePipe
  ]
})
export class ByeCalendarModule {}
