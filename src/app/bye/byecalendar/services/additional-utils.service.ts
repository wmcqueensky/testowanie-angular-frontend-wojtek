import { Injectable } from '@angular/core';
import { SYMBOLS } from '../models/constants';


export class AdditionalUtilsService {

  constructor() {}

  generateHash(len: number = 16): string {
    let hash: string = '';
    const l = SYMBOLS.length;
  
    for (let i = 0; i < len; i++) {
      hash += SYMBOLS[this.getRandomInt(0, l)];
    }
  
    return hash;
  }

  getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  getRandomColor(): string {
    return `#${this.getRandomInt(0, 255 * 255 * 255 - 1).toString(16)}`;
  }
}