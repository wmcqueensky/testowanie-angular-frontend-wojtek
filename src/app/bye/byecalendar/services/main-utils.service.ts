import { Injectable } from '@angular/core';
import { HOURS_IN_DAY, MS_IN_DAY, MS_IN_HOUR } from '../models/constants';
import { IDayCalendarHour, IDayOfMonth, IEvent, IWeekCalendarHour } from '../models/interfaces';
import { Day28Int, Day29Int, Day30Int, Day31Int, DayCalendarDay, DayCalendarRows, DayInt, DayOfWeekInt, MonthCalendarWeek, MonthCalendarWeeks, MonthInt, WeekCalendarDay, WeekCalendarDays, WeekCalendarRows } from '../models/types';


export class MainUtilsService {

  constructor() {}

  getDaysOfMonth(month: MonthInt, year?: number): IDayOfMonth[] {
    const todayDate = new Date();
    if (!year) year = todayDate.getFullYear();
  
    const days: IDayOfMonth[] = [];
    const monthWeekCount = this.getMonthWeekCount(month, year);
    const firstDayOfMonthDate = new Date(year, month, 1);
    const firstDayOfMonthTs = firstDayOfMonthDate.getTime();
    const firstDayDayOfWeekDate = new Date(firstDayOfMonthTs - MS_IN_DAY * (firstDayOfMonthDate.getDay() - 1));
    let startDate: Date;
  
    if (monthWeekCount === 4) {
      startDate = new Date(firstDayDayOfWeekDate.getTime() - 7 * MS_IN_DAY);
    } else {
      startDate = firstDayDayOfWeekDate;
    }
  
    const startTs = startDate.getTime();
  
    for (let i = 0, len = 42; i < len; i++) {
      const date = new Date(startTs + i * MS_IN_DAY);
  
      const day: IDayOfMonth = {
        day: date.getDate() as DayInt,
        month: date.getMonth() as MonthInt,
        year: date.getFullYear(),
        dayOfWeek: date.getDay() as DayOfWeekInt,
        ts: date.getTime(),
        selectedMonth: date.getMonth() === month,
        isToday: date.getFullYear() === todayDate.getFullYear()
          && date.getMonth() === todayDate.getMonth() 
          && date.getDate() === todayDate.getDate(),
        events: []
      }
  
      days.push(day);
    }
  
    return days;
  }

  getMonthWeekCount(month: MonthInt, year?: number): 4 | 5 | 6 {
    if (!year) year = new Date().getFullYear();
  
    let weekCount: number = 0;
  
    const monthDayCount = this.getMonthDayCount(month, year);
    const firstDayOfMonthDate = new Date(year, month, 1);
    const firstDayOfMonthTs = firstDayOfMonthDate.getTime();
  
    for (let i = 0; i < monthDayCount; i++) {
      if (!i) {
        ++weekCount;
        continue;
      }
  
      const date = new Date(firstDayOfMonthTs + i * MS_IN_DAY);
      if (date.getDay() === 1) ++weekCount;
    }
  
    return monthDayCount as 4 | 5 | 6;
  }

  getMonthDayCount(month: MonthInt, year?: number): DayInt {
    if (!year) year = new Date().getFullYear();
  
    const yearDayCount = this.getYearDayCount(year);
  
    if (yearDayCount === 366 && month === 1) return 29 as Day29Int;
    if (month === 1) return 28 as Day28Int;
    if ([3, 5, 8, 10].includes(month)) return 30 as Day30Int;
    return 31 as Day31Int;
  }

  getYearDayCount(year?: number): 365 | 366 {
    if (!year) year = new Date().getFullYear();
  
    return ( ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 == 0) )
      ? 366
      : 365;
  }

  getWeekCalendarDays(day: DayInt, month: MonthInt, year?: number): WeekCalendarDays {
    if (!year) year = new Date().getFullYear();
    const firstDayOfWeekDate = this.getFirstWeekDayByDay(new Date(year, month, day, 0));
    const weekCalendarDays: WeekCalendarDay[] = [];
  
    for (let i = 0; i < 7; i++) {
      const weekCalendarDay: IWeekCalendarHour[] = [];
      const dayDate = new Date(firstDayOfWeekDate.getTime() + i * MS_IN_DAY);
  
      for (let j = 0; j < 24; j++) {
        const hourDate = new Date(dayDate.getTime() + j * MS_IN_HOUR);
        weekCalendarDay.push({
          year: hourDate.getFullYear(),
          month: hourDate.getMonth() as MonthInt,
          day: hourDate.getDate() as DayInt,
          hour: hourDate.getHours(),
          dayOfWeek: hourDate.getDay() as DayOfWeekInt,
          selectedMonth: hourDate.getMonth() === month && hourDate.getFullYear() === year,
          isToday: hourDate.getMonth() === month 
            && hourDate.getFullYear() === year
            && hourDate.getDate() === day,
          ts: hourDate.getTime(),
          events: []
        });
      }
  
      weekCalendarDays.push(weekCalendarDay as WeekCalendarDay);
    }
  
    return weekCalendarDays as WeekCalendarDays;
  }

  getFirstWeekDayByDay(day: Date): Date {
    const ts = day.getTime();
    const dayOfWeek: DayOfWeekInt = day.getDay() as DayOfWeekInt;
  
    return new Date(ts - (dayOfWeek - 1) * MS_IN_DAY);
  }

  getDayCalendarDay(day: DayInt, month: MonthInt, year?: number): DayCalendarDay {
    if (!year) year = new Date().getFullYear();
  
    const dayDate = new Date(year, month, day, 0);
    const hours: IDayCalendarHour[] = [];
  
    for (let i = 0; i < HOURS_IN_DAY; i++) {
      const hourDate = new Date(dayDate.getTime() + i * MS_IN_HOUR);
  
      hours.push({
        year: hourDate.getFullYear(),
        month: hourDate.getMonth() as MonthInt,
        day: hourDate.getDate() as DayInt,
        hour: hourDate.getHours(),
        dayOfWeek: hourDate.getDay() as DayOfWeekInt,
        selectedMonth: hourDate.getMonth() === month && hourDate.getFullYear() === year,
        isToday: hourDate.getMonth() === month 
          && hourDate.getFullYear() === year
          && hourDate.getDate() === day,
        ts: hourDate.getTime(),
        events: []
      });
    }
  
    return hours as DayCalendarDay;
  }

  makeMonthCalendarWeeks(days: IDayOfMonth[]): MonthCalendarWeeks {
    const arr: MonthCalendarWeek[] = [];
  
    for (let i = 0; i < 6; i++) {
      const x: number = i * 7;
      const y: number = (i + 1) * 7;
  
      arr.push([...days.filter((_, j) => j >= x && j < y)] as MonthCalendarWeek);
    }
    
    return arr as MonthCalendarWeeks;
  }

  transformDayCalendarDayToRows(day: DayCalendarDay): DayCalendarRows {
    const rows: DayCalendarRows = [];
  
    for (let i = 0; i < 8; i++) {
      const row: IDayCalendarHour[] = [];
  
      for (let j = 0; j < 3; j++) {
        row.push(day[i * 3 + j]);
      }
  
      rows.push(row);
    }
  
    return rows;
  }

  transformWeekCalendarDaysToRows(days: WeekCalendarDays): WeekCalendarRows {
    const rows: IWeekCalendarHour[][] = [];
    
    for (let i = 0, l1 = days.length; i < l1; i++) {
      const day = days[i];
  
      for (let j = 0, l2 = day.length; j < l2; j++) {
        if (j % 4 !== 0) continue;
        
        const x = j / 4;
        if (!rows[x]) rows[x] = [];
        rows[x][i] = days[i][j];
      }
    }
    
    return rows as WeekCalendarRows;
  }

  filterEvents(events: IEvent[], start: number, end: number): IEvent[] {
    const filteredEvents: IEvent[] = [];

    for (let i = 0, len = events.length; i < len; i++) {
      const event = events[i];
      const fromTs = event.from.getTime();
      const toTs = event.to.getTime();

      const eventFromIntoRange: boolean = start < fromTs && fromTs < end;
      const eventToIntoRange: boolean = start < toTs && toTs < end;
      const rangeIntoEventRange: boolean = fromTs < end && toTs > start;

      if (eventFromIntoRange || eventToIntoRange || rangeIntoEventRange) {
        filteredEvents.push(event);
      }
    }
    
    return filteredEvents;
  }
}