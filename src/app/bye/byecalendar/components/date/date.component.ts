import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { DatepickerI18n } from '../../datepicker.i18n';

@Component({
  selector: 'glp-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
  providers: [
    {
      provide: NgbDatepickerI18n,
      useClass: DatepickerI18n
    }
  ]
})
export class GLPDateComponent implements OnInit, OnChanges {
  @ViewChild('calendarBtn') declare calendarBtnRef: ElementRef;

  @Input() model!: string;
  @Input() time: string = '00:00:00';
  @Input() inputId: string = '' + Date.now() + Math.floor(Math.random() * (9999 - 0));

  @Output() modelChange = new EventEmitter<string>();

  date_model: any;
  hour_model: any;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.model) {
      this.defineDateModel();
    }
  }

  ngOnInit() {}

  focusEmmit() {
    this.calendarBtnRef.nativeElement.click();
  }

  ngAfterViewInit() {
    this.defineDateModel();
  }

  defineDateModel() {
    let interval = setInterval(() => {
      const d = this.model;
      if (d != undefined) {
        const year = +d.substring(0, 4);
        const month = +d.substring(5, 7);
        const day = +d.substring(8, 10);

        this.date_model = {
          year: year,
          month: month,
          day: day
        }

        clearInterval(interval);
      }
    }, 100);
  }

  changed() {
    let x = '';

    if (this.date_model !== undefined) {
      x = this.date_model.year;
      x += '-';
      if (this.date_model.month < 10) { x += '0'; }
      x += this.date_model.month;
      x += '-';
      if (this.date_model.day < 10) { x += '0'; }
      x += this.date_model.day;

      if (this.hour_model !== undefined && this.hour_model !== null) {
        x += ' ';
        if (this.hour_model.hour < 10) { x += '0'; }
        x += this.hour_model.hour;
        x += ':';
        if (this.hour_model.minute < 10) { x += '0'; }
        x += this.hour_model.minute;
      } else {
        x += ' 00:00';
      }
    }
    
    this.modelChange.emit(x.substring(0, 11) + this.time);
  }
}