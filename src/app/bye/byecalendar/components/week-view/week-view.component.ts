import { MS_IN_HOUR } from './../../models/constants';
import { MainUtilsService } from '../../services/main-utils.service';
import { MonthInt, DayInt, WeekCalendarDays, WeekCalendarRows } from '../../models/types';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ICeilClickEvent, IEvent, IWeekCalendarHour } from '../../models/interfaces';
import I18nCalendarConfigDTO from '../../dtos/I18nCalendarConfig.dto';


const todayDate = new Date();

@Component({
  selector: 'glp-calendar-week-view',
  templateUrl: './week-view.component.html',
  styleUrls: ['./week-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WeekViewComponent implements OnInit, OnChanges {
  @Input() year: number = todayDate.getFullYear();
  @Input() month: MonthInt = todayDate.getMonth() as MonthInt;
  @Input() day: DayInt = todayDate.getDate() as DayInt;
  @Input() events: IEvent[] = [];
  @Input() configI18n: I18nCalendarConfigDTO = new I18nCalendarConfigDTO();

  @Output() OnCeilClick = new EventEmitter<ICeilClickEvent>();

  declare weekCalendarRows: WeekCalendarRows;
  headers: string[] = this.configI18n.daysOfWeek;
  headersShort: string[] = this.configI18n.daysOfWeekShort;

  constructor(private mainUtils: MainUtilsService) {}

  ngOnChanges() {
    this.getWeekCalendarRows();
  }

  ngOnInit(): void {
    this.configI18n = new I18nCalendarConfigDTO(this.configI18n);
  }

  onCeilClick(col: IWeekCalendarHour) {
    const from = new Date(col.ts);
    const to = new Date(col.ts + MS_IN_HOUR * 4 - 1);

    this.OnCeilClick.emit({
      range: {from, to},
      viewMode: 'month'
    });
  }

  private getWeekCalendarRows() {
    const weekCalendarDays: WeekCalendarDays = this.mainUtils.getWeekCalendarDays(this.day, this.month, this.year);
    const rows: WeekCalendarRows = this.mainUtils.transformWeekCalendarDaysToRows(weekCalendarDays);

    for (let i = 0, l1 = rows.length; i < l1; i++) {
      const row = rows[i];

      for (let j = 0, l2 = row.length; j < l2; j++) {
        const col = row[j];
        col.events = this.getEvents(col);
      }
    }

    this.weekCalendarRows = rows;
  }

  getEvents(col: IWeekCalendarHour): IEvent[] {
    const start = new Date(col.year, col.month, col.day, col.hour, 0, 0).getTime();
    const end = new Date(start + MS_IN_HOUR * 4 - 1).getTime();

    return this.mainUtils.filterEvents(this.events, start, end);
  }
}