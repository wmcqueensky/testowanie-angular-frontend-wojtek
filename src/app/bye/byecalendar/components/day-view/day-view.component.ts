import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';
import I18nCalendarConfigDTO from '../../dtos/I18nCalendarConfig.dto';
import { MS_IN_HOUR } from '../../models/constants';
import { ICeilClickEvent, IDayCalendarHour, IEvent, IWeekCalendarHour } from '../../models/interfaces';
import { DayCalendarDay, DayCalendarRows, DayInt, MonthInt } from '../../models/types';
import { MainUtilsService } from '../../services/main-utils.service';


const todayDate = new Date();

@Component({
  selector: 'glp-calendar-day-view',
  templateUrl: './day-view.component.html',
  styleUrls: ['./day-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DayViewComponent implements OnInit, OnChanges {
  @Input() year: number = todayDate.getFullYear();
  @Input() month: MonthInt = todayDate.getMonth() as MonthInt;
  @Input() day: DayInt = todayDate.getDate() as DayInt;
  @Input() events: IEvent[] = [];
  @Input() configI18n: I18nCalendarConfigDTO = new I18nCalendarConfigDTO();
  
  @Output() OnCeilClick = new EventEmitter<ICeilClickEvent>();

  declare dayCalendarRows: DayCalendarRows;

  constructor(private mainUtils: MainUtilsService) {}

  ngOnChanges(): void {
    this.getDayCalendarRows();
  }

  ngOnInit(): void {
    this.configI18n = new I18nCalendarConfigDTO(this.configI18n);
  }

  onCeilClick(col: IWeekCalendarHour) {
    const from = new Date(col.ts);
    const to = new Date(col.ts + MS_IN_HOUR - 1);

    this.OnCeilClick.emit({
      range: {from, to},
      viewMode: 'month'
    });
  }

  private getDayCalendarRows() {
    const dayCalendarDay: DayCalendarDay = this.mainUtils.getDayCalendarDay(this.day, this.month, this.year);
    const rows: DayCalendarRows = this.mainUtils.transformDayCalendarDayToRows(dayCalendarDay);

    for (let i = 0, l1 = rows.length; i < l1; i++) {
      const row = rows[i];

      for (let j = 0, l2 = row.length; j < l2; j++) {
        const col = row[j];
        col.events = this.getEvents(col);
      }
    }

    this.dayCalendarRows = rows;
  }

  private getEvents(col: IDayCalendarHour): IEvent[] {
    const start = new Date(col.year, col.month, col.day, col.hour, 0, 0).getTime();
    const end = new Date(start + MS_IN_HOUR - 1).getTime();

    return this.mainUtils.filterEvents(this.events, start, end);
  }
}