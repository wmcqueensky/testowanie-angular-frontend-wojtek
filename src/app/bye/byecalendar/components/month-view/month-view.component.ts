import { MS_IN_DAY } from './../../models/constants';
import { ICeilClickEvent } from './../../models/interfaces';
import { MainUtilsService } from '../../services/main-utils.service';
import { MonthCalendarWeeks, MonthInt, DayInt } from '../../models/types';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { IDayOfMonth, IEvent } from '../../models/interfaces';
import { DayEventsFilterPipe } from '../../pipes/day-events-filter.pipe';
import I18nCalendarConfigDTO from '../../dtos/I18nCalendarConfig.dto';


const todayDate = new Date();

@Component({
  selector: 'glp-calendar-month-view',
  templateUrl: './month-view.component.html',
  styleUrls: ['./month-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MonthViewComponent implements OnInit, OnChanges {
  @Input() year: number = todayDate.getFullYear();
  @Input() month: MonthInt = todayDate.getMonth() as MonthInt;
  @Input() day: DayInt = todayDate.getDate() as DayInt;
  @Input() events: IEvent[] = [];
  @Input() configI18n: I18nCalendarConfigDTO = new I18nCalendarConfigDTO();

  @Output() OnCeilClick = new EventEmitter<ICeilClickEvent>();

  declare monthCalendarRows: MonthCalendarWeeks;
  headers: string[] = this.configI18n.daysOfWeek;
  shortHeaders: string[] = this.configI18n.daysOfWeekShort;

  constructor(
    private dayEventsFilter: DayEventsFilterPipe,
    private mainUtils: MainUtilsService
  ) {}

  ngOnChanges() {
    this.getMonthCalendarRows();
  }

  ngOnInit(): void {
    this.configI18n = new I18nCalendarConfigDTO(this.configI18n);
  }

  onCeilClick(col: IDayOfMonth) {
    const from = new Date(col.ts);
    const to = new Date(col.ts + MS_IN_DAY - 1);

    this.OnCeilClick.emit({
      range: {from, to},
      viewMode: 'month'
    });
  }

  private getMonthCalendarRows() {
    const days: IDayOfMonth[] = this.mainUtils.getDaysOfMonth(this.month, this.year);
    const rows: MonthCalendarWeeks = this.mainUtils.makeMonthCalendarWeeks(days);

    for (let i = 0, l1 = rows.length; i < l1; i++) {
      const row = rows[i];

      for (let j = 0, l2 = row.length; j < l2; j++) {
        const day = row[j];
        day.events = this.getDayEvents(day);
      }
    }

    this.monthCalendarRows = rows;
  }

  private getDayEvents(day: IDayOfMonth): IEvent[] {
    return this.dayEventsFilter.transform(this.events, day.year, day.month, day.day);
  }
}