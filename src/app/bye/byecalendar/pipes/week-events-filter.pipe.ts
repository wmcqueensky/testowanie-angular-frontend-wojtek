import { MS_IN_WEEK } from './../models/constants';
import { DayInt } from './../models/types';
import { Pipe, PipeTransform } from '@angular/core';
import { IEvent } from '../models/interfaces';
import { MonthInt } from '../models/types';
import { MainUtilsService } from '../services/main-utils.service';

@Pipe({
  name: 'weekEventsFilter'
})
export class WeekEventsFilterPipe implements PipeTransform {

  constructor(private mainUtils: MainUtilsService) {}

  transform(events: IEvent[], year: number, month: MonthInt, day: DayInt): IEvent[] {
    const firstWeekDayDate = this.mainUtils.getFirstWeekDayByDay(new Date(year, month, day, 0, 0));
    const start: number = firstWeekDayDate.getTime();
    const end: number = new Date(start + MS_IN_WEEK - 1).getTime();

    return this.mainUtils.filterEvents(events, start, end);

  }
}