import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeRangeFromHour'
})
export class TimerangeFromHourPipe implements PipeTransform {

  transform(h: number): {from: string, to: string} {
    const x: string | number = h <= 9 ? '0' + h : h;
    return {
      from: `${x}:00`,
      to: `${x}:59`
    };
  }

}
