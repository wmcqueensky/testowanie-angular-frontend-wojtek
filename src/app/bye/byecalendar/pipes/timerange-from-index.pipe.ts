import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeRangeFromIndex'
})
export class TimerangeFromIndexPipe implements PipeTransform {
  
  transform(i: number): {from: string, to: string} {
    const y = i * 4;
    const z = y + 3;
    const x1: string | number = y <= 9 ? '0' + y : y;
    const x2: string | number = z <= 9 ? '0' + z : z;

    return {
      from: `${x1}:00`,
      to: `${x2}:59`
    }
  }
}