import { Pipe, PipeTransform } from '@angular/core';
import { IEvent } from '../models/interfaces';
import { DayInt, MonthInt } from '../models/types';
import { MainUtilsService } from '../services/main-utils.service';

@Pipe({
  name: 'dayEventsFilter'
})
export class DayEventsFilterPipe implements PipeTransform {

  constructor(private mainUtils: MainUtilsService) {}

  transform(events: IEvent[], year: number, month: MonthInt, day: DayInt): IEvent[] {
    const start: number = new Date(year, month, day, 0, 0, 0).getTime();
    const end: number = new Date(year, month, day, 23, 59, 59).getTime();
    
    return this.mainUtils.filterEvents(events, start, end);
  }
}