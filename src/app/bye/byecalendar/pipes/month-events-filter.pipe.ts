import { IEvent } from './../models/interfaces';
import { Pipe, PipeTransform } from '@angular/core';
import { MonthInt } from '../models/types';
import { MainUtilsService } from '../services/main-utils.service';

@Pipe({
  name: 'monthEventsFilter'
})
export class MonthEventsFilterPipe implements PipeTransform {

  constructor(private mainUtils: MainUtilsService) {}

  transform(events: IEvent[], year: number, month: MonthInt): IEvent[] {
    const monthDayCount: number = this.mainUtils.getMonthDayCount(month, year);
    const start: number = new Date(year, month, 0, 0, 0).getTime();
    const end: number = new Date(year, month, monthDayCount, 59, 59, 59).getTime();

    return this.mainUtils.filterEvents(events, start, end);
  }
}