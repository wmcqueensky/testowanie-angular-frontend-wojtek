import { DayInt, DayOfWeekInt, EventPeriodType, MonthInt, ViewMode } from "./types";


export interface IDayOfMonth {
  day: DayInt, 
  month: MonthInt, 
  year: number, // year YYYY,
  dayOfWeek: DayOfWeekInt, // 0 - Sunday
  selectedMonth: boolean, // is not additional day
  isToday: boolean, // or is today
  ts: number, // timeswamp
  events: IEvent[]
}

export interface IWeekCalendarHour extends IDayOfMonth {
  hour: number,
}

export interface IDayCalendarHour extends IWeekCalendarHour {}

export interface IEvent {
  id?: string,
  key?: string | number,
  title: string,
  periodType: EventPeriodType,
  from: Date,
  to: Date,
  color: string
}

export interface ICeilClickEvent {
  range: {
    from: Date,
    to: Date
  },
  viewMode: ViewMode
}

export interface IViewEventsModel {
  year: number,
  month: MonthInt,
  day: DayInt,
  events: IEvent[]
}

export interface IRemoveEvent {
  key?: string | number,
  id: string
}

export interface IEventEditorModel extends IEvent {}