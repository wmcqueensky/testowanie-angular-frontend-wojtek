export const MS_IN_SECOND: number = 1000;
export const MS_IN_MINUTE: number = MS_IN_SECOND * 60;
export const MS_IN_HOUR: number = MS_IN_MINUTE * 60;
export const MS_IN_DAY: number = MS_IN_HOUR * 24;
export const MS_IN_WEEK: number = MS_IN_DAY * 7;
export const MS_IN_38DAYS: number = MS_IN_DAY * 28;
export const MS_IN_29DAYS: number = MS_IN_DAY * 29;
export const MS_IN_30DAYS: number = MS_IN_DAY * 30;
export const MS_IN_31DAYS: number = MS_IN_DAY * 31;
export const MS_IN_365DAYS: number = MS_IN_DAY * 365;
export const MS_IN_366DAYS: number = MS_IN_DAY * 366;

export const HOURS_IN_DAY: number = 24;

export const MONTHES_NAME: string[] = ['BYECALENDAR_JANUARY', 'BYECALENDAR_FEBRUARY', 'BYECALENDAR_MARCH', 'BYECALENDAR_APRIL', 'BYECALENDAR_MAY', 'BYECALENDAR_JUNE', 'BYECALENDAR_JULY', 'BYECALENDAR_AUGUST', 'BYECALENDAR_SEPTEMBER', 'BYECALENDAR_OCTOBER', 'BYECALENDAR_NOVEMBER', 'BYECALENDAR_DECEMBER'];
export const MONTHES_SHORT_NAME: string[] = ['BYECALENDAR_JAN', 'BYECALENDAR_FEB', 'BYECALENDAR_MAR', 'BYECALENDAR_APR', 'BYECALENDAR_MAY', 'BYECALENDAR_JUN', 'BYECALENDAR_JUL', 'BYECALENDAR_AUG', 'BYECALENDAR_SEP', 'BYECALENDAR_OCT', 'BYECALENDAR_NOV', 'BYECALENDAR_DEC'];
export const DAYS_OF_WEEK_NAME: string[] = ['BYECALENDAR_MONDAY', 'BYECALENDAR_TUESDAY', 'BYECALENDAR_WEDNESDAY', 'BYECALENDAR_THURSDAY', 'BYECALENDAR_FRIDAY', 'BYECALENDAR_SATURDAY', 'BYECALENDAR_SUNDAY'];
export const DAYS_OF_WEEK_SHORT_NAME: string[] = ['BYECALENDAR_MON', 'BYECALENDAR_TUE', 'BYECALENDAR_WED', 'BYECALENDAR_THU', 'BYECALENDAR_FRI', 'BYECALENDAR_SAT', 'BYECALENDAR_SUN'];

export const SYMBOLS: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];