import { IHelper, HelperType, ValidateOn, IHelperItem } from './../validator.service';
import { Component, OnInit, Input, ElementRef, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorService } from '../validator.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-byetime',
  templateUrl: './byetime.component.html',
  styleUrls: ['./byetime.component.scss']
})
export class ByetimeComponent implements OnChanges, OnInit {
  @ViewChild('dateTimeModal', { static: false }) dateTimeModal: ElementRef<HTMLElement>;
  @ViewChild('calendarBtn') calendarBtnRef: ElementRef;

  @Input() model: any;
  @Input() helper: IHelper;
  @Input() key: string;
  @Input() valid: HelperType;
  @Input() showtime = true;
  @Input() showpicker = true;
  @Input() disable = false;
  @Input() validateOn: ValidateOn[] = ['modelChange'];

  date_model: any;
  hour_model: any;
  fresh: boolean = true;

  constructor(
    private validator: ValidatorService, 
    private modalService: NgbModal,
    private utils: UtilsService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.valid && this.helper && this.key && this.helper[this.key]) {
      const helperItem: IHelperItem = this.utils.copy(this.helper[this.key]);
      
      this.helper[this.key] = {
        type: this.valid,
        valid: helperItem?.valid || 'UNKNOWN',
        hide: helperItem?.hide || false,
        wasFocused: helperItem?.wasFocused || false,
        lostFocus: helperItem?.lostFocus || false,
        wasChanged: helperItem?.wasChanged || false,
        modelWasChanged: helperItem?.modelWasChanged || false,
        isValid: helperItem?.isValid || false,
        isInvalid: helperItem?.isInvalid || false
      }

      if (this.validateOn.includes('modelChange') || this.validateOn.includes('input')) {
        this.validate();
      }
    }
  }

  ngOnInit() {
    if (this.helper && this.key) {
      this.helper[this.key] = {
        type: this.valid,
        valid: 'UNKNOWN',
        hide: false,
        wasFocused: false,
        lostFocus: false,
        wasChanged: false,
        modelWasChanged: false,
        isValid: false,
        isInvalid: false
      };

      if (this.validateOn.includes('init')) {
        this.validate();
      }
    }
  }

  focusEmmit() {
    this.calendarBtnRef.nativeElement.click();
  }

  ngAfterViewInit() {
    const d = this.model[this.key];
    if (d === undefined) return;
    const year = +d.substring(0, 4);
    const month = +d.substring(5, 7);
    const day = +d.substring(8, 10)

    window.setTimeout(() =>
      this.date_model = {
        year: year,
        month: month,
        day: day
      }
    );
  }

  changed() {
    let x = '';

    if (this.date_model !== undefined) {
      x = this.date_model.year;
      x += '-';
      if (this.date_model.month < 10) { x += '0'; }
      x += this.date_model.month;
      x += '-';
      if (this.date_model.day < 10) { x += '0'; }
      x += this.date_model.day;

      if (this.hour_model !== undefined && this.hour_model !== null) {
        x += ' ';
        if (this.hour_model.hour < 10) { x += '0'; }
        x += this.hour_model.hour;
        x += ':';
        if (this.hour_model.minute < 10) { x += '0'; }
        x += this.hour_model.minute;
      } else {
        x += ' 00:00';
      }
    }

    this.model[this.key] = x;
    this.validate();
  }

  public openDialog() {
    this.modalService.open(this.dateTimeModal, { size: 'sm', ariaLabelledBy: 'modal-basic-title', windowClass: "timeWindow" });
  }

  public validate() {
    this.validator.updateHelperWithKey(this.key, this.model, this.helper, false);
  }
}
