import { IHelper, HelperType, ValidateOn, IHelperItem } from './../validator.service';
import { Component, OnInit, Input, ElementRef, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ValidatorService } from '../validator.service';
import { DatepickerI18n } from 'src/app/datepicker.i18n';
import { UtilsService } from 'src/app/services/utils.service';


const NOW_DATE = new Date();


const DEFAULT_START_NGB_DATE: NgbDateStruct = {
  year: NOW_DATE.getFullYear(),
  month: NOW_DATE.getMonth() + 1,
  day: NOW_DATE.getDate()
}


const DEFAULT_MIN_NGB_DATE: NgbDateStruct = {
  year: NOW_DATE.getFullYear() - 75,
  month: 1,
  day: 1
}


const DEFAULT_MAX_NGB_DATE: NgbDateStruct = {
  year: NOW_DATE.getFullYear() + 50,
  month: 12,
  day: 31
}


@Component({
  selector: 'app-byedate',
  templateUrl: './byedate.component.html',
  styleUrls: ['./byedate.component.scss'],
  providers: [
    {
      provide: NgbDatepickerI18n,
      useClass: DatepickerI18n
    }
  ]
})
export class ByedateComponent implements OnChanges, OnInit {
  @ViewChild('calendarBtn') calendarBtnRef: ElementRef;

  @Input() model: any;
  @Input() helper: IHelper;
  @Input() key: string;
  @Input() valid: HelperType;
  @Input() showtime = true;
  @Input() showpicker = true;
  @Input() disable = false;
  @Input() time: string = '00:00:00';
  @Input() start?: number | string | Date;
  @Input() min?: number | string | Date;
  @Input() max?: number | string | Date;
  @Input() validateOn: ValidateOn[] = ['modelChange'];

  date_model: any;
  hour_model: any;
  fresh: boolean = true;

  _startNgbDate?: NgbDateStruct;
  _minNgbDate?: NgbDateStruct;
  _maxNgbDate?: NgbDateStruct;

  private mounted: boolean = false;

  constructor(
    private validator: ValidatorService, 
    private readonly utils: UtilsService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.valid && this.helper && this.key && this.helper[this.key]) {
      const helperItem: IHelperItem = this.utils.copy(this.helper[this.key]);
      
      this.helper[this.key] = {
        type: this.valid,
        valid: helperItem?.valid || 'UNKNOWN',
        hide: helperItem?.hide || false,
        wasFocused: helperItem?.wasFocused || false,
        lostFocus: helperItem?.lostFocus || false,
        wasChanged: helperItem?.wasChanged || false,
        modelWasChanged: helperItem?.modelWasChanged || false,
        isValid: helperItem?.isValid || false,
        isInvalid: helperItem?.isInvalid || false
      }

      if (this.validateOn.includes('modelChange') || this.validateOn.includes('input')) {
        this.validate();
      }
    }

    let x: any;
    
    try {
      x = this.model[this.key as string];
    } catch (_) {}

    if (changes.start || !this.mounted) {
      this._startNgbDate = this.datetimeToNgbDateStruct(changes.start?.currentValue)
        || this.datetimeToNgbDateStruct(x)
        || this.utils.copy(DEFAULT_START_NGB_DATE);
    }


    if (changes.min || !this.mounted) {
      this._minNgbDate = this.datetimeToNgbDateStruct(changes.min?.currentValue) 
        || this.utils.copy(DEFAULT_MIN_NGB_DATE);
    }

    if (changes.max || !this.mounted) {
      this._maxNgbDate = this.datetimeToNgbDateStruct(changes.max?.currentValue) 
        || this.utils.copy(DEFAULT_MAX_NGB_DATE);
    }

    this.mounted = true;
  }

  ngOnInit() {
    this.fresh = true;
    if (this.helper && this.key) {
      this.helper[this.key] = {
        type: this.valid,
        valid: 'UNKNOWN',
        hide: false,
        wasFocused: false,
        lostFocus: false,
        wasChanged: false,
        modelWasChanged: false,
        isValid: false,
        isInvalid: false
      };

      if (this.validateOn.includes('init')) {
        this.validate();
      }
    }

    this.changed();
  }

  focusEmmit() {
    this.calendarBtnRef.nativeElement.click();
  }

  ngAfterViewInit() {
    let interval = setInterval(() => {
      const d = this.model[this.key];
      if(d != undefined) {
        const year = +d.substring(0, 4);
        const month = +d.substring(5, 7);
        const day = +d.substring(8, 10);

        this.date_model = {
          year: year,
          month: month,
          day: day
        }

        clearInterval(interval);
      }
    },100);      
  }

  changed() {
    let x = '';

    if (this.date_model !== undefined) {
      x = this.date_model.year;
      x += '-';
      if (this.date_model.month < 10) { x += '0'; }
      x += this.date_model.month;
      x += '-';
      if (this.date_model.day < 10) { x += '0'; }
      x += this.date_model.day;

      if (this.hour_model !== undefined && this.hour_model !== null) {
        x += ' ';
        if (this.hour_model.hour < 10) { x += '0'; }
        x += this.hour_model.hour;
        x += ':';
        if (this.hour_model.minute < 10) { x += '0'; }
        x += this.hour_model.minute;
      } else {
        x += ' 00:00';
      }
    }
    
    this.model[this.key] = x.substring(0, 11) + this.time;
    this.validate();
  }

  public validate() {
    this.validator.updateHelperWithKey(this.key, this.model, this.helper, false);
  }

  private datetimeToNgbDateStruct(datetime: number | string | Date | undefined | null): NgbDateStruct | undefined {
    if (!datetime && datetime !== 0) return undefined;
    const date = this.utils.newDate(datetime);

    return {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    }
  }
}
