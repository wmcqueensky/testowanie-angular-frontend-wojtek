import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NgxImageCompressService } from 'ngx-image-compress';
import { BasicService } from 'src/app/services/basic.service';
import { AlertsService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-byeimageuploader',
  templateUrl: './byeimageuploader.component.html',
  styleUrls: ['./byeimageuploader.component.scss']
})
export class ByeimageuploaderComponent {
  @ViewChild('uploadModal') uploadModal: HTMLElement;

  @Input() model;
  @Input() helper;
  @Input() key: string;
  @Input() uploader: string;
  @Input() type: string;
  @Input() disable = false;
  @Input() max = 10;
  @Input() min = 0;
  @Input() format;

  @Input() uploadBtnText: string = 'UPLOADER_UPLOAD_BUTTON_TEXT';
  @Input() saveBtnText: string = 'UPLOADER_SAVE_BUTTON_TEXT';
  @Input() header: string = 'UPLOADER_DOWNLOAD_BUTTON_TEXT';

  @Input() modalOpenerClass: string = 'btn-primary';
  @Input() uploadBtnClass: string = 'btn-primary';
  @Input() saveBtnClass: string = 'btn-success';

  @Input() compressRatio: number = 75;
  @Input() compressQuality: number = 50;

  @Output() OnSuccess: EventEmitter<any> = new EventEmitter<any>();
  @Output() OnError: EventEmitter<any> = new EventEmitter<any>();

  loading: boolean = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageURL: any = '';
  file: string;

  modalReference: NgbModalRef;

  constructor(
    private modalService: NgbModal,
    private http: HttpClient,
    private basic: BasicService,
    public alert: AlertsService,
    private imageCompress: NgxImageCompressService
  ) {}

  uploadAndCompress() {
    this.imageURL = '';

    this.imageCompress.uploadFile()
      .then(({image, orientation}) => {

        this.imageCompress.compressFile(image, orientation, this.compressRatio, this.compressQuality)
          .then(result => {
            this.file = result;
            this.imageURL = result;
          });
      });
  }

  fileChangeEvent(event: any): void {
    this.file = event.target.files[0];
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded(image: HTMLImageElement) {
    
  }

  cropperReady() {
    
  }

  loadImageFailed() {
    
  }

  open(content) {
    this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
    this.modalReference
      .result
      .then((result) => {}, (reason) => {});
  }

  openUploadModal() {
    this.imageURL = '';
    this.croppedImage = '';
    this.open(this.uploadModal);
  }

  upload() {
    if (!this.croppedImage) return;
    if (!this.uploader) return;

    const list = this.model[this.key];

    if(list !== undefined && list !== null && list.length >= this.max) {
      this.alert.showInfo('','UPLOAD_SERVICE_CANNOT_UPLOAD_MORE_FILES');
      return;
    }

    const file = this.croppedImage;

    this.alert.showLoading('UPLOAD_SERVICE_LOADING_MESSAGE');

    let name: string = '' + Date.now() + '.jpg';
    const fg: FormData = new FormData;
    fg.append('file', dataURLtoFile(file, name));
    fg.append('token', this.basic.getToken());

    const uploader: string = this.uploader.trim()[0] === '/' 
      ? this.uploader.trim() 
      : `/${this.uploader.trim()}`;

    this.http.post(`${this.basic.getBaseUrl()}${uploader}`, fg)
      .subscribe((res: any) => {
        try {
          this.loading = false;
          this.OnSuccess.emit(res.data);
          this.modalReference.close();

          const km = 'metadata';
          const ks = 'status';
          const kd = 'data';
          const metadata = res[km];
          const data = res[kd];
          if (metadata == null || metadata === undefined) {
            this.alert.showError('', 'UPLOAD_SERVICE_FAILURE_MESSAGE');
            return;
          }

          const status = metadata[ks];
          if (status !== 'OK') {
            this.alert.showError('', 'UPLOAD_SERVICE_FAILURE_MESSAGE');
            return;
          }

          let list = this.model[this.key];
          if (list == null || list === undefined) {
            this.model[this.key] = [];
            list = this.model[this.key];
          }

          list.push(data);
          this.alert.showTimeout('UPLOAD_SERVICE_SUCCESS_MESSAGE');
        } catch (err) {
          this.loading = false;
          this.OnError.emit(err);
        }
      }, err => {
        console.error(err);
        this.loading = false;
        this.OnError.emit(err);
      });
  }
}

function getBlob(basa64Data: string): string {
  const img = document.createElement('img');
  img.setAttribute('src', 'data:image/jpg;base64,' + basa64Data);
  return img.src;
}

function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), 
      n = bstr.length, 
      u8arr = new Uint8Array(n);
      
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  
  return new File([u8arr], filename, {type:mime});
}
