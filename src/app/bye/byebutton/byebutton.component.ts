import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bye-button',
  templateUrl: './byebutton.component.html',
  styleUrls: ['./byebutton.component.scss']
})
export class ByebuttonComponent {
  @Input() className: string = '';
  @Input() loaderClassName: string = '';
  @Input() spinnerClassNames: string[] = ['', '', ''];
  @Input() loading: boolean = false;
  @Input() typeName: 'button' | 'submit' = 'button';
  @Input() disabled: boolean = false;
  
  @Output() OnClick = new EventEmitter<MouseEvent>();

  constructor() {}

  onClick(e: MouseEvent) {
    this.OnClick.emit(e);
  }
}
