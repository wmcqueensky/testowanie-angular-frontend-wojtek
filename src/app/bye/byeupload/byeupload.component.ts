import { LoaderService } from "./../../services/loader.service";
import {
  Component,
  ElementRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { BasicService } from "src/app/services/basic.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AlertsService } from "src/app/services/alert.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-byeupload",
  templateUrl: "./byeupload.component.html",
  styleUrls: ["./byeupload.component.scss"],
})
export class ByeuploadComponent implements OnInit {
  @Input() model: any; //
  @Input() key: string; //
  @Input() uploader: string; //
  @Input() saver: string; //
  @Input() max: number;
  @Input() min: number = 0;
  @Input() format: string;
  @Input() buttontext: string = "UPLOAD_BUTTON_TEXT";

  @Output() OnUpload = new EventEmitter();

  @ViewChild("uploader", { static: false })
  uploaderButton: ElementRef<HTMLElement>;
  @ViewChild("uploaderModal", { static: false })
  uploaderModal: ElementRef<HTMLElement>;

  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  private baseUrl;

  fileInfos: Observable<any>;

  constructor(
    private http: HttpClient,
    private basic: BasicService,
    private modalService: NgbModal,
    private alert: AlertsService,
    private translator: TranslateService,
    private loader: LoaderService
  ) {}
  ngOnInit(): void {
    if (this.uploader !== undefined && this.uploader !== null) {
      if (this.uploader.startsWith("/"))
        this.baseUrl = this.basic.getBaseUrl() + this.uploader;
      else this.baseUrl = this.basic.getBaseUrl() + "/" + this.uploader;
    }
  }

  click() {
    if (
      this.max !== undefined &&
      this.max > 0 &&
      this.key != undefined &&
      this.model != undefined &&
      this.model[this.key] != undefined &&
      this.model[this.key].length >= this.max
    ) {
      this.alert.showInfo("", "UPLOAD_LIMIT_REACHED");
      return;
    }

    this.uploaderButton.nativeElement.click();
  }

  upload(event): void {
    this.selectedFiles = event.target.files;
    this.progress = 0;
    const dialog = this.openLoadingDialog();

    this.currentFile = this.selectedFiles.item(0);
    this.loader.state = {
      showLoader: true,
    };

    this.uploadService(this.currentFile).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((100 * event.loaded) / event.total);
        } else if (event instanceof HttpResponse) {
          this.modalService.dismissAll();
          if (this.model == null || this.model == undefined) return;
          let list = this.model[this.key];
          if (list == null || list == undefined) {
            list = [];
            this.model[this.key] = list;
          }
          list.push(event.body["data"]);
          this.OnUpload.emit();
          this.loader.state = {
            showLoader: false,
          };

          this.saveAndUpdateModel();
        }
      },
      (err) => {
        this.modalService.dismissAll();
        this.progress = 0;
        this.currentFile = undefined;

        this.loader.state = {
          showLoader: false,
        };
      }
    );

    this.selectedFiles = undefined;
  }

  uploadService(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append("file", file);
    formData.append("token", this.basic.getToken());

    const req = new HttpRequest("POST", `${this.baseUrl}`, formData, {
      reportProgress: true,
      responseType: "json",
    });

    return this.http.request(req);
  }

  public openLoadingDialog() {
    this.modalService
      .open(this.uploaderModal, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {},
        (reason) => {}
      );
  }

  saveAndUpdateModel() {
    if (this.saver == null || this.saver === undefined) {
      return;
    }
    const req = {
      metadata: {
        token: this.basic.getToken(),
      },
      data: this.model,
    };

    let url;
    if (this.saver.startsWith("/")) url = this.basic.getBaseUrl() + this.saver;
    else url = this.basic.getBaseUrl() + "/" + this.saver;

    this.loader.state = {
      showLoader: true,
    };

    this.http.post(url, req).subscribe(
      (res) => {
        const status = this.basic.processResponse(res);
        if (status === "OK") {
          const data = res["data"];
          if (data != null && data != undefined) {
            const keys = Object.keys(data);
            const n = keys.length;
            for (let i = 0; i < n; i++) {
              const key = keys[i];
              this.model[key] = data[key];
            }
          }
        } else {
        }

        this.loader.state = {
          showLoader: false,
        };
      },
      (err) => {
        this.loader.state = {
          showLoader: true,
        };
      }
    );
  }
}
