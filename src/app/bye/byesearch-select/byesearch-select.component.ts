import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { BasicService } from 'src/app/services/basic.service';

interface IOption {
  value: number,
  description: string
}

interface IEvent {
  originalEvent?: Event,
  value: number,
  description: string
}

@Component({
  selector: 'app-byesearch-select',
  templateUrl: './byesearch-select.component.html',
  styleUrls: ['./byesearch-select.component.scss']
})
export class ByesearchSelectComponent implements OnInit {
  @ViewChild('input') inputRef: ElementRef<HTMLElement>;

  @Input() value: number = null;
  @Input() options: IOption[] = [];
  @Input() servlet: string = null;
  @Input() resetOn: string[] = [];
  @Input() showAddBtn: boolean = true;
  @Input() showAddBtnText: string = 'Dodaj'; 
  @Input() searchResultCount: number = 5;
  @Input() label: string = '';

  @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() OnFocus: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  @Output() OnBlur: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  @Output() OnInput: EventEmitter<number> = new EventEmitter<number>();
  @Output() OnEnter: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  @Output() OnSelect: EventEmitter<IEvent> = new EventEmitter<IEvent>();

  hash: string = '' + Date.now();
  selectedOption: IOption;
  showDropdown: boolean = false;
  timer: number;

  filteredOptions: IOption[] = [];

  constructor(
    private http: HttpClient,
    private basic: BasicService
  ) {}

  ngOnInit(): void {
    
  }

  selectOption(value: number) {
    this.selectedOption = this.options.find(opt => opt.value === value);
    this.value = this.toNumber(this.selectedOption.value);
    this.valueChange.emit(this.value);
    this.showDropdown = false;

    this.OnSelect.emit({...this.selectedOption});
  }

  onFocus(e: Event | any) {
    this.showDropdown = true;

    const val: string = e.target.value || '';
    this.defineFilteredOptions(val);

    if (!this.selectedOption) {
      this.OnFocus.emit({
        originalEvent: e,
        value: this.value,
        description: null
      });
      return;
    }

    this.OnFocus.emit({
      originalEvent: e,
      value: this.selectedOption?.value,
      description: this.selectedOption?.description
    });
  }

  onBlur(e: Event | any) {
    setTimeout(() => {
      this.showDropdown = false;
      if (!this.selectedOption) {
        this.OnBlur.emit({
          originalEvent: e,
          value: this.value,
          description: null
        });
        return;
      }

      this.OnBlur.emit({
        originalEvent: e,
        value: this.selectedOption?.value,
        description: this.selectedOption?.description
      });
    }, 300);
  }

  onInput(e: Event | any) {
    this.showDropdown = true;
    const val: string = e.target.value || '';
    this.defineFilteredOptions(val);

    this.value = this.toNumber(val);
    this.valueChange.emit(this.value);

    this.OnInput.emit(this.value);

    if (this.timer) {
      window.clearTimeout(this.timer);
    }
    this.timer = window.setTimeout(() => {
      // this.getOptions();
    }, 500);
  }

  onEnter(e: Event | any) {
    this.showDropdown = false;
    this.selectedOption = this.options.find(opt => opt.value === this.value);

    if (!this.selectedOption) {
      this.OnEnter.emit({
        originalEvent: e,
        value: this.value,
        description: null
      });

      return;
    }

    this.OnEnter.emit({
      originalEvent: e,
      value: this.selectedOption?.value,
      description: this.selectedOption?.description
    });

    if (this.resetOn.includes('ENTER')) {
      this.selectedOption = null;
      this.value = null;
      this.valueChange.emit(this.value);
    }
  }

  // getOptions() {
  //   if (!this.servlet) return;

  //   const req: any = {
  //     data: {
  //       filter: {
  //         hint: this.value
  //       },
  //       pagination: {
  //         start_page: 0,
  //         max_page_length: 5,
  //         asc: true
  //       }
  //     }
  //   }

  //   this.http.post(`${this.basic.getBaseUrl()}${this.servlet}`, req)
  //     .subscribe((res: any) => {
  //       const status = this.basic.processResponse(res);
  //       if (status !== 'OK') throw new Error(status);

  //       this.options = res.data.list;
  //     }, err => {
  //       console.error(err);
  //     });
  // }

  emitEnter() {
    this.inputRef.nativeElement.dispatchEvent(new KeyboardEvent('keyup',{'key':'enter'}));
  }

  toNumber(x: number | string): number {
    return x && !isNaN(+x) ? +x : null;
  }

  getOptions(): IOption[] {
    return this.filteredOptions.filter((_, i) => i < this.searchResultCount);
  }

  private defineFilteredOptions(val: string) {
    let filteredOptions: IOption[] = [];
    
    if (!val || !val.trim().length) {
      filteredOptions = this.options;
    } else {
      filteredOptions = this.options.filter(opt => opt.description.includes(val));
    }

    this.filteredOptions = JSON.parse(JSON.stringify(filteredOptions));
  }
}