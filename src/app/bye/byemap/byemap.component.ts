import { ByemapService, ITransferData, IMinMaxLatLng } from './byemap.service';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';
import {} from 'google.maps';
import MarkerClusterer from '@googlemaps/markerclustererplus';
import { Cluster } from '@googlemaps/markerclustererplus/dist/cluster';

declare const google: any;


interface IConfig {
  apiKey?: string,
  version?: string,
  language?: string,
  region?: string
}

export interface ILatLng {
  lat: number,
  lng: number
}

export interface ICoords extends ILatLng {
  zoom: number
}

export interface IMarker extends ILatLng {
  draggable?: boolean,
  key?: string | number,
  icon?: string
}

export interface ICircle extends ILatLng {
  radius: number,
  key?: string | number,
}

export interface IMapEvent {
  latLng: ILatLng,
  originalEvent: google.maps.MapMouseEvent
}

export interface IMarkerEvent {
  latLng: ILatLng,
  key: string | number,
  originalEvent: google.maps.IconMouseEvent
}

export interface IClusterEvent {
  originalEvent: Cluster,
  markers: IMarker[],
}

export interface ICircleEvent {
  originalEvent: any,
  key?: number | string,
  bounds?: IMinMaxLatLng
}

const defaultCoords: ICoords = {
  lat: 53.8169683,
  lng: 21.741278,
  zoom: 11
}


@Component({
  selector: 'app-byemap',
  templateUrl: './byemap.component.html',
  styleUrls: ['./byemap.component.scss']
})
export class ByemapComponent implements OnInit {
  @ViewChild('map') mapElement: ElementRef<HTMLElement>;
  
  @Input() className: string = ''
  @Input() config: IConfig = {};
  @Input() coords: ICoords = null;
  @Input() markers: IMarker[] = [];
  @Input() circles: ICircle[] = [];
  @Input() polylinePoints: ILatLng[] | ILatLng[][] = [];
  @Input() key?: string = 'map';

  @Input() mapClickCb: Function = () => {};
  @Input() markerClickCb: Function = () => {};
  @Input() markerDragStartCb: Function = () => {};
  @Input() markerDragEndCb: Function = () => {};
  @Input() circleClickCb: Function = () => {};
  @Input() context: any;

  @Input() needUseClasterer: boolean = false;
  @Input() needFocusOnAllMarkers: boolean = false;

  @Output() OnMapClick: EventEmitter<IMapEvent> = new EventEmitter<IMapEvent>();
  @Output() OnMarkerClick: EventEmitter<IMarkerEvent> = new EventEmitter<IMarkerEvent>();
  @Output() OnMarkerDragStart: EventEmitter<IMarkerEvent> = new EventEmitter<IMarkerEvent>();
  @Output() OnMarkerDragEnd: EventEmitter<IMarkerEvent> = new EventEmitter<IMarkerEvent>();
  @Output() OnClusterClick: EventEmitter<IClusterEvent> = new EventEmitter<IClusterEvent>();
  @Output() OnCircleClick: EventEmitter<ICircleEvent> = new EventEmitter<ICircleEvent>();

  private loader: Loader = new Loader({
    apiKey: this.config.apiKey || "AIzaSyDqDgizTljsaqcbdL6xjF3luzYU6HvdJf4",
    version: this.config.version || "weekly",
    language: this.config.language || 'pl',
    region: this.config.region || 'PL'
  });

  private map: google.maps.Map;
  private markersOnMap: google.maps.Marker[] = [];
  private circlesOnMap: google.maps.Circle[] = [];

  private polyline: google.maps.Polyline[] = [];

  //private interval: number;

  private colors: string[] = ['#0D6EFD', '#6C757D', '#198754', '#DC3545', '#FFC107', '#FFC107', '#F8F9FA', '#212529'];

  private markerCluster: MarkerClusterer;

  constructor(
    private byemap: ByemapService
  ) {
    if (!this.coords) {
      this.setDefaultCoords();
    }
  }

  ngOnInit() {
    this.loader.load()
      .then(() => {
        this.initMap();
        this.listen();
      });
  }

  ngOnChanges(): void {   
    this.setDefaultCoords();
    this.clearMarkers();
    this.clearCircles();
    this.makeMarkers();
    this.makePath();
    this.makeCircles();
  }

  listen() {
    this.byemap.getTransferData$()
      .subscribe((data: ITransferData) => {
        if (!this.key) return;

        if (data?.key === this.key && data?.action === 'INIT') {
          
        }

        if (data?.key === this.key && data?.action === 'FIT_BOUNDS') {
          window.setTimeout(() => {
            const bounds = data?.payload?.bounds;
          
            try {
              this.map.fitBounds(bounds);
            } catch (err) {
              console.error(err)
            }
          }, 1000);
        }
      });
  }

  private setDefaultCoords() {
    this.coords = {
      ...defaultCoords,
      ...(this.coords || {})
    }
  }

  private initMap() {
    try {
      const mapOptions: google.maps.MapOptions = {
        center: {
          lat: this.coords.lat,
          lng: this.coords.lng,
        },
        zoom: this.coords.zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapId: this.key,
        maxZoom: 18,
        minZoom: 3,
        fullscreenControl: false
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      this.makeMarkers();
      this.makePath();
      this.makeCircles();

      this.map.addListener('click', (e: google.maps.MapMouseEvent) => {
        const mapEvent: IMapEvent = {
          latLng: {
            lat: e.latLng.lat(),
            lng: e.latLng.lng(),
          },
          originalEvent: e
        }

        this.OnMapClick.emit(mapEvent);
        this.mapClickCb.call(this.context, e);
      });
    } catch (err) {
      console.error(err);
    }
  }

  makeMarkers() {
    this.markers.forEach(m => {
      try {
        const marketConfig: any = {
          position: {
            lat: m.lat,
            lng: m.lng
          },
          draggable: m.draggable,
          map: this.map
        }
        
        if (m.icon) {
          marketConfig.icon = m.icon;
        }

        let marker: google.maps.Marker = new google.maps.Marker(marketConfig);

        marker.addListener('click', (e: google.maps.IconMouseEvent) => {
          this.OnMarkerClick.emit({
            latLng: {
              lat: e.latLng.lat(),
              lng: e.latLng.lng(),
            },
            originalEvent: e,
            key: m.key
          });
          this.markerClickCb.call(this.context, e, m.key);
        });

        marker.addListener('dragstart', (e: google.maps.IconMouseEvent) => {
          this.OnMarkerDragStart.emit({
            latLng: {
              lat: e.latLng.lat(),
              lng: e.latLng.lng(),
            },
            originalEvent: e,
            key: m.key
          });
          this.markerDragStartCb.call(this.context, e, m.key);
        });

        marker.addListener('dragend', (e: google.maps.IconMouseEvent) => {
          this.OnMarkerDragEnd.emit({
            latLng: {
              lat: e.latLng.lat(),
              lng: e.latLng.lng(),
            },
            originalEvent: e,
            key: m.key
          });
          this.markerDragEndCb.call(this.context, e, m.key);
        });

        this.markersOnMap.push(marker);

        const bounds: [[number, number], [number, number]] = this.combineBounds();
        let LatLngBounds: google.maps.LatLngBounds;
        try {
          LatLngBounds = new google.maps.LatLngBounds(
            {lat: bounds[1][0], lng: bounds[1][1]},
            {lat: bounds[0][0], lng: bounds[0][1]}
          );
        } catch (_) {}
        
        try {
          this.map.fitBounds(LatLngBounds);
        } catch (_) {}
        
      } catch (e) {}
    });

    if (this.needFocusOnAllMarkers) {
      try {
        const minMaxLatLng: IMinMaxLatLng = this.byemap.getMinMaxLatLng(this.markers);
        
        if (minMaxLatLng) {
          this.map.fitBounds({
            south: minMaxLatLng.minLat, 
            north: minMaxLatLng.maxLat,
            west: minMaxLatLng.minLng,
            east: minMaxLatLng.maxLng
          });
        }
      } catch (_) {console.error(_)}
    }

    if (!this.needUseClasterer) return;

    this.markerCluster = new MarkerClusterer(
      this.map, 
      this.markersOnMap,
      {imagePath: `https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m`}
    );

    this.markerCluster.addListener('click', (e: Cluster) => {
      const googleMarkers: any[] = e.getMarkers() || [];
      const markers: IMarker[] = googleMarkers.map(m => {
        const lat: number = m.getPosition().lat();
        const lng: number = m.getPosition().lng();
        const draggable: boolean = m.getDraggable();
        const key: number | string = m.key

        return {lat, lng, draggable, key};
      });

      this.OnClusterClick.emit({
        originalEvent: e,
        markers
      });
    });
  }

  clearMarkers() {
    this.markersOnMap.forEach(m => {
      m.setMap(null);
    });

    this.markersOnMap = [];
  }

  makeCircles() {
    this.circles.forEach(c => {
      const circleConfig: any = {
        center: {
          lat: c.lat,
          lng: c.lng
        },
        map: this.map,
        key: c.key,
        radius: c.radius
      }

      let circle: google.maps.Circle = new google.maps.Circle(circleConfig);
      const bounds: any = circle.getBounds();
      const minMaxLatLng: IMinMaxLatLng = {
        minLat: bounds.lc.g,
        maxLat: bounds.lc.i,
        minLng: bounds.Eb.g,
        maxLng: bounds.Eb.i,
      }

      circle.addListener('click', (e: google.maps.IconMouseEvent) => {
        this.OnCircleClick.emit({
          originalEvent: e,
          key: c.key,
          bounds: minMaxLatLng
        });
        this.markerClickCb.call(this.context, e, c.key);
      });
    });
  }

  clearCircles() {
    this.circlesOnMap.forEach(c => {
      c.setMap(null);
    });

    this.circlesOnMap = [];
  }

  makePath() {
    this.polyline = this.polyline.map(p => {
      p.setMap(null);
      return p;
    });

    this.polyline = [];

    let latlngs: number[][] = this.normalizePathPoints();

    try {
      if (!latlngs) return;

      latlngs.forEach((latLng, idx) => {
        const flightPlanCoordinates: any = latLng.map(ll => {
          return {
            lat: ll[0],
            lng: ll[1],
          }
        });
        
        try {
          const polyline: google.maps.Polyline = new google.maps.Polyline({
            path: flightPlanCoordinates,
            strokeColor: this.colors[idx],
            strokeOpacity: 1,
            strokeWeight: 2
          });

          polyline.setMap(this.map);

          this.polyline.push(polyline);
        } catch (err) {}
      });
      
      const bounds: [[number, number], [number, number]] = this.combineBounds();
      let LatLngBounds: google.maps.LatLngBounds;
      try {
        LatLngBounds = new google.maps.LatLngBounds(
          {lat: bounds[1][0], lng: bounds[1][1]},
          {lat: bounds[0][0], lng: bounds[0][1]}
        );
      } catch (_) {}
      
      try {
        this.map.fitBounds(LatLngBounds);
      } catch (_) {}
    } catch (err) {
      console.error(err);
    }
  }

  private combineBounds(): [[number, number], [number, number]] {
    const latLngs = this.normalizePathPoints();
    const coords: ILatLng[] = this.getCoordsFromNormalizedPathPoints(latLngs);
    this.markers.forEach(m => {
      coords.push({
        lat: m.lat,
        lng: m.lng
      });
    });
    const bounds: [[number, number], [number, number]] = this.byemap.makeBounds(coords);

    return bounds;
  }

  private normalizePathPoints() {
    let latlngs: any[] | any[][] = [];
    const polylinePoints: ILatLng[] | Array<ILatLng[]> = this.polylinePoints;

    try {
      if (polylinePoints.some(point => typeof(point) !== 'object')) throw new Error('Points must be a object or array');

      let isSimpleArray: boolean = false;
      let isMultiArray: boolean = false;
       
      try {
        isSimpleArray = (polylinePoints as any[]).every(point => !Array.isArray(point));
      } catch (_) {
        isSimpleArray = false;
      }

      try {
        isMultiArray = (polylinePoints as any[]).every(point => Array.isArray(point));
      } catch (_) {
        isMultiArray = false;
      }

      if (!isSimpleArray && !isMultiArray) throw new Error('polylinePoints must by or points or array of points, but not mixed');

      if (isSimpleArray) {
        latlngs = (polylinePoints as ILatLng[]).map(point => {
          return [point.lat, point.lng];
        });
      }

      if (isMultiArray) {
        latlngs = (polylinePoints as Array<ILatLng[]>).map(pointGroup => {
          return pointGroup.map(point => {
            return [point.lat, point.lng];
          });
        });
      }
      
      if (!latlngs.length) return null;

      if (isSimpleArray) {
        latlngs = [latlngs];
      }

      return latlngs;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  private getCoordsFromNormalizedPathPoints(latLngs): ILatLng[] {
    let coords = [];
    latLngs.forEach(latLng => {
      coords = [...coords, ...latLng]
    });

    return coords.map(coord => {
      return {
        lat: coord[0],
        lng: coord[1]
      }
    });
  }
}