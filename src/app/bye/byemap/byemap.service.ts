import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { ILatLng } from './byemap.component';

type Action = 'INIT' | 'FIT_BOUNDS';

export interface ITransferData {
  action: Action,
  key?: string,
  payload?: any
}

export interface IMinMaxLatLng {
  minLat: number,
  maxLat: number,
  minLng: number,
  maxLng: number,
} 

@Injectable({
  providedIn: 'root'
})
export class ByemapService {
  private data: ITransferData;
  private data$: Subject<ITransferData>;
  
  constructor() {
    this.data$ = new BehaviorSubject<ITransferData>(this.data);
  }

  getTransferData$(): Observable<ITransferData> {
    return this.data$.asObservable();
  }

  // Tymczasowo zablokowane
  private setTransferData(data: ITransferData): void {
    this.data = JSON.parse(JSON.stringify(data));
    this.data$.next(this.data);
  }

  makeBounds(latLngs: ILatLng[]): [[number, number], [number, number]] {
    const lats: number[] = [];
    const lngs: number[] = [];

    latLngs.forEach(latLng => {
      lats.push(+latLng.lat);
      lngs.push(+latLng.lng);
    });

    const northEastLat: number = Math.max(...lats);
    const northEastLng: number = Math.max(...lngs);
    const southWestLat: number = Math.min(...lats);
    const southWestLng: number = Math.min(...lngs);

    return [[northEastLat + 0.15, northEastLng + 0.15], [southWestLat - 0.15, southWestLng - 0.15]]
  }

  getDistanceMinMaxLatLng(latLng: ILatLng, distance: number): Promise<IMinMaxLatLng> {
    return new Promise<IMinMaxLatLng>((res, rej) => {
      let interval: number;
      let i: number = 0;
      const circleConfig: any = {
        center: {
          lat: latLng.lat,
          lng: latLng.lng
        },
        radius: distance
      }

      try {
        interval = window.setInterval(() => {
          if (!google) throw new Error('google is not defined');
          let circle: google.maps.Circle = new google.maps.Circle(circleConfig);
          const bounds: any = circle.getBounds();
          
          window.clearInterval(interval);

          return res({
            minLat: bounds.lc.g,
            maxLat: bounds.lc.i,
            minLng: bounds.Eb.g,
            maxLng: bounds.Eb.i,
          });
        }, 100);
      } catch (err) {
        ++i;
        if (i < 15) return;
        window.clearInterval(interval);
        rej(err);
      }
    });
  }

  async getDistanceBetweenPoints(point1: ILatLng, point2: ILatLng): Promise<number> {

    const R = 6371000; // promień Ziemi w matrach
    const a1 = point1.lat * Math.PI/180; // szerokość geograficzna w radianach
    const a2 = point2.lat * Math.PI/180;
    const da = (point2.lat - point1.lat) * Math.PI/180;
    const dl = (point2.lng - point1.lng) * Math.PI/180;
    
    const a = Math.sin(da/2) * Math.sin(da/2) +
              Math.cos(a1) * Math.cos(a2) *
              Math.sin(dl/2) * Math.sin(dl/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    
    return R * c; // odległość w metrach
  }

  getMinMaxLatLng(coords: ILatLng[]): IMinMaxLatLng | null {
    if (!coords?.length) return null;
    coords = coords.filter(coord => coord.lat && coord.lng);

    const lats: number[] = coords.map(coord => coord.lat);
    const lngs: number[] = coords.map(coord => coord.lng);

    if (!lats?.length || !lngs.length) return null;

    return {
      minLat: Math.min(...lats),
      maxLat: Math.max(...lats),
      minLng: Math.min(...lngs),
      maxLng: Math.max(...lngs),
    }
  }
}