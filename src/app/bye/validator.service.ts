import { Injectable } from "@angular/core";
import { AlertsService } from "../services/alert.service";

export type HelperType = 'NON_EMPTY' | 'ZIPCODE_PL' | 'ZIP_CODE' | 'POSITIVE' | 'NEGATIVE' | 'NON_POSITIVE' | 'NON_NEGATIVE' | 'PESEL' | 'NIP' | 'REGON' | 'KRS' | 'EMAIL' | 'PHONE' | 'HOUR' | 'HOUR_OR_EMPTY' | 'NONE' | string; // MIN_LENGTH_X | MAX_LENGTH_X | MIN_X | MAX_X | EMPTY_OR_YYY

export type ValidateOn = 'init' | 'focus' | 'blur' | 'input' | 'modelChange';

export interface IHelperItem {
  type?: HelperType
  valid?: 'VALID' | 'INVALID' | 'UNKNOWN',
  hide?: boolean,
  wasFocused?: boolean,
  lostFocus?: boolean,
  wasChanged?: boolean,
  modelWasChanged?: boolean,
  isInvalid?: boolean,
  isValid?: boolean
}

export interface IHelper {
  [key: string]: IHelperItem
}

const VALID = 'VALID';
const INVALID = 'INVALID';
const UNKNOWN = 'UNKNOWN';
const k = 'valid';

const HOUR_REG: string = '^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$';

@Injectable({
  providedIn: "root",
})
export class ValidatorService {

  constructor(private alert: AlertsService) {}

  private validateEmpty(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    sub[k] = VALID;
    sub.isInvalid = false;
    sub.isValid = true;
  }

  private validateNonEmpty(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];

    if (!value) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    }
  }

  private validateMinLength(key: string, model: any, sub: IHelperItem, strict: boolean = false, minLength: number = 0) {
    let value = model[key];
    if (!value) value = '';

    if (value.length < minLength) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true
    }
  }

  private validateMaxLength(key: string, model: any, sub: IHelperItem, strict: boolean = false, maxLength: number = Infinity) {
    let value = model[key];
    if (!value) value = '';

    if (value.length > maxLength) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true
    }
  }

  private validateZipcodePl(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];
    const reg = new RegExp("^[0-9]{2}-[0-9]{3}$");

    if (!reg.test(value)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    }
  }

  private validatePositive(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    this.validateMin(key, model, sub, strict, 1);
  }

  private validateNegative(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    this.validateMax(key, model, sub, strict, -1);
  }

  private validateNonPositive(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    this.validateMax(key, model, sub, strict, 0);
  }

  private validateNonNegative(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    this.validateMin(key, model, sub, strict, 0);
  }

  private validateMin(key: string, model: any, sub: IHelperItem, strict: boolean = false, min: number = 0) {
    let value = +model[key];

    if (value >= min) {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateMax(key: string, model: any, sub: IHelperItem, strict: boolean = false, max: number = 0) {
    let value = +model[key];

    if (value <= max) {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validatePesel(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const pesel = model[key];
    const reg = new RegExp("^[0-9]{11}$");

    if (!pesel || !reg.test(pesel)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
      return;
    }

    const digits = ("" + pesel).split("");

    if (
      parseInt(pesel?.substring(4, 6), 10) > 31 ||
      parseInt(pesel?.substring(2, 4), 10) > 12
    ) {
      // Sprawdzanie, czy nie wpisano numeru dnia lub miesiąca spoza zakresu
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
      return;
    }

    // Sprawdzanie sumy kontrolnej
    let checksum =
      (1 * parseInt(digits[0], 10) +
        3 * parseInt(digits[1], 10) +
        7 * parseInt(digits[2], 10) +
        9 * parseInt(digits[3], 10) +
        1 * parseInt(digits[4], 10) +
        3 * parseInt(digits[5], 10) +
        7 * parseInt(digits[6], 10) +
        9 * parseInt(digits[7], 10) +
        1 * parseInt(digits[8], 10) +
        3 * parseInt(digits[9], 10)) %
      10;

    if (checksum === 0) {
      checksum = 10;
    }
    checksum = 10 - checksum;

    if (parseInt(digits[10], 10) === checksum) {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateNIP(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const nip = model[key];
    const reg = new RegExp("^[0-9]{10}$");

    if (!nip || !reg.test(nip)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
      return;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
      return;
    }
  }

  private validateRegon(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const regon = model[key];
    const reg = new RegExp("^[0-9]{9}$");

    if (!regon || !reg.test(regon)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
      return;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
      return;
    }
  }

  private validateKRS(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const krs = model[key];
    const reg = new RegExp("^[0-9]{1,10}$");

    if (!krs || !reg.test(krs)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
      return;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
      return;
    }
  }

  private validateEmail(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( !value || !reg.test(value.toLowerCase()) ) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    }
  }

  private validatePhone(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];
    const reg = new RegExp("^(\\+[0-9]{2-3})?[0-9]{9}$");

    if (!value || !reg.test(value)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    }
  }

  private validateHour(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];
    const reg = new RegExp(HOUR_REG);

    if (!value || !reg.test(value)) {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    } else {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    }
  }

  private validateHourOrEmpty(key: string, model: any, sub: IHelperItem, strict: boolean = false) {
    const value = model[key];
    const reg = new RegExp(HOUR_REG);

    if (!value) {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else if (reg.test(value)) {
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateTrue(key?: string, model?: any, sub?: IHelperItem, strict: boolean = false) {
    const value = model[key];

    if (value) { 
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateFalse(key?: string, model?: any, sub?: IHelperItem, strict: boolean = false) {
    const value = model[key];

    if (!value) { 
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateStrictTrue(key?: string, model?: any, sub?: IHelperItem, strict: boolean = false) {
    const value = model[key];

    if (value === true) { 
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private validateStrictFalse(key?: string, model?: any, sub?: IHelperItem, strict: boolean = false) {
    const value = model[key];

    if (value === false) { 
      sub[k] = VALID;
      sub.isInvalid = false;
      sub.isValid = true;
    } else {
      sub[k] = strict ? INVALID : UNKNOWN;
      sub.isInvalid = true;
      sub.isValid = false;
    }
  }

  private makeValid(key?: string, model?: any, sub?: IHelperItem, strict: boolean = false) {
    sub[k] = UNKNOWN;
    sub.isInvalid = false;
    sub.isValid = false;
  }

  public updateHelperWithKey(key: string, model: any, helper: IHelper, strict: boolean = false, disposableType?: string) {
    if (!model || !helper || typeof(model) !== 'object' || typeof(helper) !== 'object') {
      return;
    }
    
    const sub = helper[key];
    if (!sub) {
      return;
    }

    const type: string | null = disposableType || sub?.type;
    
    const MIN_LENGTH_ = 'MIN_LENGTH_';
    const MAX_LENGTH_ = 'MAX_LENGTH_';
    const MIN_ = 'MIN_';
    const MAX_ = 'MAX_';
    const EMPTY_OR_ = 'EMPTY_OR_';
    
    if (type?.substr(0, EMPTY_OR_.length) === EMPTY_OR_) {

      if (!model[key]) {
        this.validateEmpty(key, model, sub, strict);
      } else {
        this.updateHelperWithKey(key, model, helper, strict, type?.substr(EMPTY_OR_.length))
      }
    } else if (type === 'NON_EMPTY') {
      this.validateNonEmpty(key, model, sub, strict);
    } else if (type?.substr(0, MIN_LENGTH_.length) === MIN_LENGTH_) {
      this.validateMinLength(key, model, sub, strict, +type?.substr(MIN_LENGTH_.length));
    } else if (type?.substr(0, MAX_LENGTH_.length) === MAX_LENGTH_) {
      this.validateMaxLength(key, model, sub, strict, +type?.substr(MAX_LENGTH_.length));
    } else if (type === 'ZIPCODE_PL') {
      this.validateZipcodePl(key, model, sub, strict);
    } else if (type === 'ZIP_CODE') {
      this.validateZipcodePl(key, model, sub, strict);
    } else if (type === 'POSITIVE') {
      this.validatePositive(key, model, sub, strict);
    } else if (type === 'NEGATIVE') {
      this.validateNegative(key, model, sub, strict);
    } else if (type === 'NON_POSITIVE') {
      this.validateNonPositive(key, model, sub, strict);
    } else if (type === 'NON_NEGATIVE') {
      this.validateNonNegative(key, model, sub, strict);
    } else if (type?.substr(0, MIN_.length) === MIN_) {
      this.validateMin(key, model, sub, strict, +type?.substr(MIN_.length));
    } else if (type?.substr(0, MAX_.length) === MAX_) {
      this.validateMax(key, model, sub, strict, +type?.substr(MAX_.length));
    } else if (type === 'PESEL') {
      this.validatePesel(key, model, sub, strict);
    } else if (type === 'NIP') {
      this.validateNIP(key, model, sub, strict);
    } else if (type === 'REGON') {
      this.validateRegon(key, model, sub, strict);
    } else if (type === 'KRS') {
      this.validateKRS(key, model, sub, strict);
    } else if (type === 'EMAIL') {
      this.validateEmail(key, model, sub, strict);
    } else if (type === 'PHONE') {
      this.validatePhone(key, model, sub, strict);
    } else if (type === 'HOUR') {
      this.validateHour(key, model, sub, strict);
    } else if (type === 'HOUR_OR_EMPTY') {
      this.validateHourOrEmpty(key, model, sub, strict);
    } else if (type === 'TRUE') {
      this.validateTrue(key, model, sub, strict);
    } else if (type === 'FALSE') {
      this.validateFalse(key, model, sub, strict);
    } else if (type === 'STRICT_TRUE') {
      this.validateStrictTrue(key, model, sub, strict);
    } else if (type === 'STRICT_FALSE') {
      this.validateStrictFalse(key, model, sub, strict);
    } else if (type === 'NONE') {
      this.makeValid(key, model, sub, strict);
    } else {
      this.makeValid(key, model, sub, strict);
    }
  }

  testHelper(helper: IHelper): boolean {
    const keys = Object.keys(helper);
    const n = keys.length;

    for (let i = 0; i < n; i++) {
      const key = keys[i];
      const type = helper[key].type;

      if (type === undefined || type === "NONE") {
        continue;
      }

      if (helper[key].valid === INVALID) {
        return false;
      }
    }

    return true;
  }

  public strictValidate(model: any, helper: IHelper): boolean {
    if (!model || !helper || typeof(model) !== 'object' || typeof(helper) !== 'object') {
      return false;
    }

    const keys = Object.keys(helper);
    const n = keys.length;

    // Ustawianie kolorów kontrolek
    for (let i = 0; i < n; i++) {
      this.updateHelperWithKey(keys[i], model, helper, true);
    }

    // Testowanie poprawności modelu
    if (!this.testHelper(helper)) {
      this.alert.showError("", "CORRECT_RED_FIELDS");
      return false;
    }

    return true;
  }
}
