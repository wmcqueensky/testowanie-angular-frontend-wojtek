import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[NumberFilter]'
})
export class NumbersonlyDirective {

    @Input() isInteger: any;
    @Input() isSigned: any;
    @Input() decimalSeparator = '.';
    @Input() decimalDigits: any;
    @Input() integerDigits: any;

    // Domyślne wartości wyrażeń regularnych

    private integerUnsigned = '^[0-9]+(\\.[0-9]+)?$';
    private integerSigned = '^-?[0-9]+(\\.[0-9]+)?$';
    private decimalUnsigned = '^[0-9]+(\\.[0-9]+)?$';
    private decimalSigned = '^-?[0-9]+(\\.[0-9]+)?$';

    private previousValue = '';

    constructor(private hostElement: ElementRef) {
    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit(): void {

        if (this.integerDigits > 0) {
            if (this.decimalDigits > 0) {
                this.decimalUnsigned = '^[0-9]{0,' + this.integerDigits + '}(\.[0-9]{0,' + this.decimalDigits + '})?$';
                this.decimalSigned = '^-?[0-9]{0,' + this.integerDigits + '}(\.[0-9]{0,' + this.decimalDigits + '})?$';
            } else {
                this.decimalUnsigned = '^[0-9]{0,' + this.integerDigits + '}(\.[0-9]+)?$';
                this.decimalSigned = '^-?[0-9]{0,' + this.integerDigits + '}(\.[0-9]+)?$';
            }
        } else {
            if (this.decimalDigits === undefined) {
                this.decimalUnsigned = '^[0-9]+(\.[0-9]{0,' + this.decimalDigits + '})?$';
                this.decimalSigned = '^-?[0-9]+(\.[0-9]{0,' + this.decimalDigits + '})?$';
            }
            else if (this.decimalDigits > 0) {
                this.decimalUnsigned = '^[0-9]+(\.[0-9]{0,' + this.decimalDigits + '})?$';
                this.decimalSigned = '^-?[0-9]+(\.[0-9]{0,' + this.decimalDigits + '})?$';
            }
            else {
                this.decimalUnsigned = '^[0-9]+$';
                this.decimalSigned = '^-?[0-9]+$';
            }
        }
    }

    @HostListener('change', ['$event']) onChange(e) {
        this.checkPastedText(this.hostElement.nativeElement.value);
    }

    @HostListener('paste', ['$event']) onPaste(e) {
        const value = e.clipboardData.getData('text/plain');
        this.checkPastedText(value);
        e.preventDefault();
    }

    @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent) {

        // tslint:disable-next-line: no-string-literal
        let cursorPosition: number = e.target['selectionStart'];
        // tslint:disable-next-line: no-string-literal
        const originalValue: string = e.target['value'];
        const key: string = this.getName(e);
        const controlOrCommand = (e.ctrlKey === true || e.metaKey === true);
        const signExists = originalValue.includes('-');
        const separatorExists = originalValue.includes(this.decimalSeparator);

        const allowedKeys = [
            'Backspace', 'ArrowLeft', 'ArrowRight', 'Escape', 'Tab'
        ];

        if (!separatorExists) {
            allowedKeys.push(this.decimalSeparator);
        }

        const firstCharacterIsSeparator = (originalValue.charAt(0) !== this.decimalSeparator);
        if (this.isSigned && !signExists &&
            firstCharacterIsSeparator && cursorPosition === 0) {
            allowedKeys.push('-');
        }

        if (allowedKeys.indexOf(key) !== -1 ||
            (key === 'a' && controlOrCommand) ||
            (key === 'c' && controlOrCommand) ||
            (key === 'v' && controlOrCommand) ||
            (key === 'x' && controlOrCommand)) {
            return;
        }

        this.previousValue = originalValue;

        if ((originalValue === '0' || originalValue === '-0') && cursorPosition > 0 && key >= '0' && key <= '9') {
            e.preventDefault();
            return;
        }

        let helper: any;
        if (originalValue.startsWith('-')) {
            helper = originalValue.substring(1);
            cursorPosition--;
        } else {
            helper = originalValue;
        }

        if (this.integerDigits > 0 && cursorPosition === this.integerDigits) {
            if ((new RegExp('^[0-9]{' + this.integerDigits + '}.*$')).test(helper)) {
                e.preventDefault();
                return;
            }
        }

        const separatorIndex = originalValue.indexOf(this.decimalSeparator);
        if (separatorIndex >= 0) {
            const dec = originalValue.substring(separatorIndex + 1);
            if ((new RegExp('^[0-9]{' + this.decimalDigits + '}$')).test(dec)) {
                e.preventDefault();
                return;
            }
        }

        const isNumber = (new RegExp(this.integerUnsigned)).test(key);
        if (isNumber) {
            return;
        } else {
            e.preventDefault();
        }
    }

    checkPastedText(value: string): void {
        let regex: string;
        if (this.isInteger && !this.isSigned) { regex = this.integerUnsigned; }
        else if (this.isInteger && this.isSigned) { regex = this.integerSigned; }
        else if (!this.isInteger && !this.isSigned) { regex = this.decimalUnsigned; }
        else if (!this.isInteger && this.isSigned) { regex = this.decimalSigned; }
        else { regex = this.decimalSigned; }

        const firstCharacter = value.charAt(0);
        if (firstCharacter === this.decimalSeparator) {
            value = 0 + value;
        }

        const lastCharacter = value.charAt(value.length - 1);
        if (lastCharacter === this.decimalSeparator) {
            value = value + 0;
        }

        const valid: boolean = (new RegExp(regex)).test(value);
        const x = valid ? value : 0;
        // tslint:disable: no-string-literal
        // tslint:disable: triple-equals
        // tslint:disable-next-line: no-unused-expression
        //this.hostElement.nativeElement['value'] == valid ? value : 0;
        this.hostElement.nativeElement['value'] = x;
    }

    getName(e: any): string {

        if (e.key) {
            return e.key;
        } else {
            if (e.keyCode && String.fromCharCode) {
                switch (e.keyCode) {
                    case 8: return 'Backspace';
                    case 9: return 'Tab';
                    case 27: return 'Escape';
                    case 37: return 'ArrowLeft';
                    case 39: return 'ArrowRight';
                    case 188: return '.';
                    case 190: return '.';
                    case 109:
                    case 173:
                    case 189: return '-'; // różne odmiany minusa
                    default: return String.fromCharCode(e.keyCode);
                }
            }
        }
    }
}
