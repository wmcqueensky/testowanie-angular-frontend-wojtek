import { Injectable } from '@angular/core';


interface IValidateParams {
  model: Object | any,
  key: string,
  validators: any[],
  validationState: Object | any
}

export interface IValState {
  [key: string]: any
}

@Injectable({
  providedIn: 'root'
})
export class InputValidatorService {

  constructor() {}

  validate(validationParams: IValidateParams): void {
    try {
      if (validationParams.validationState === 'UNKNOWN') return;

      let {model, key, validators, validationState} = validationParams;
      let result = validators.map( f => f(model[key]) );
      result = result.filter(r => r !== null);

      this.setStateAndKey(validationState, key);

      if (result.length === 0) {
        validationState[key].errors = null;
        validationState[key].valid = true;
        validationState[key].invalid = false;
      } else {
        validationState[key]['errors'] = [...result];
        validationState[key].valid = false;
        validationState[key].invalid = true;
      }

      this.checkValidState(validationState);
    } catch (e) {}
  }

  inputMounted(validState: Object | any, key: string): void {
    if (validState === 'UNKNOWN') return;

    this.setStateAndKey(validState, key);

    validState[key].untouched = true;
    validState[key].touched = false;
    validState[key].pristine = true;
    validState[key].dirty = false;
  }

  inputFocused(validState: Object | any, key: string): void {
    if (validState === 'UNKNOWN') return;

    this.setStateAndKey(validState, key);

    validState[key].untouched = false;
    validState[key].touched = true;
  }

  inputChanged(validState: Object | any, key: string): void {
    if (validState === 'UNKNOWN') return;

    this.setStateAndKey(validState, key);

    validState[key].pristine = false;
    validState[key].dirty = true;
  }

  setObj(obj: Object): Object {
    return obj ? obj : {};
  }

  private checkValidState(validState: Object | any): void {
    for (let key in validState) {
      if (validState[key].invalid) {
        validState.invalid = true;
        validState.valid = false;
        return;
      }
    }

    validState.invalid = false;
    validState.valid = true;
  }

  private setStateAndKey(validState, key) {
    validState = this.setObj(validState);
    validState[key] = this.setObj(validState[key]);
  }
}