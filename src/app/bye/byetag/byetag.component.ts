import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-byetag',
  templateUrl: './byetag.component.html',
  styleUrls: ['./byetag.component.scss']
})
export class ByetagComponent implements OnInit {
  @Input() text: string = '';
  @Input() className: string = '';
  @Input() key: any = {};
  @Input() letClose: boolean = true;

  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  close() {
    this.onClose.emit(this.key);
  }
}
