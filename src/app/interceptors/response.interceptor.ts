import { GlobalActions } from './../classes/GlobalActions';
import { BasicService } from 'src/app/services/basic.service';
import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable, tap } from "rxjs";
import Swal from 'sweetalert2';



@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  
  constructor(
    private router: Router,
    private basic: BasicService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const cloned: HttpRequest<any> = req.clone();
    return next.handle(cloned)
      .pipe(
        map(event => {
          if (event.type === HttpEventType.Response && event.ok && event.body) {
            const {metadata} = event.body;
            if (metadata?.status === 'MAIN_PASS_NOT_FOUND') {
              GlobalActions.state = {action: 'MAIN_PASS_NOT_FOUND'};
              delete metadata.token;
            }
          }

          return event;
        })
      );
  }
}