import { ListOfProductsComponent } from './pages/list-of-products/list-of-products.component';
import { ProductEditorComponent } from './pages/product-editor/product-editor.component';
import { UserDashboardComponent } from './pages/user-dashboard/user-dashboard.component';
import { AdministratorDashboardComponent } from './pages/administrator-dashboard/administrator-dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthComponent } from './auth/auth.component';
import { MatLayoutComponent } from './layouts/mat-layout/mat-layout.component';
import { GullLayoutComponent } from './layouts/gull-layout/gull-layout.component';
import { Routes, RouterModule } from '@angular/router';
import { FinishRegisterComponent } from './auth/finish-register/finish-register.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ResetComponent } from './auth/reset/reset.component';
import { UnfinishedRegistrationComponent } from './auth/unfinished-registration/unfinished-registration.component';
import { roleGuard } from './guards/role.guard';
import { FinishResetComponent } from './auth/finish-reset/finish-reset.component';
import {MapComponent} from './components/map/map.component';

const appRoutes: Routes = [
	{ path: '', component: AuthComponent, children: [
		{path: '', redirectTo: 'login', pathMatch: 'full'},
    	{path: 'login', component: LoginComponent},
    	{path: 'register', component: RegisterComponent},
    	{path: 'reset', component: ResetComponent},
		{path: 'finish_reset', component: FinishResetComponent},
    	{path: 'finish_register', component: FinishRegisterComponent},
    	{path: 'unfinished_registration', component: UnfinishedRegistrationComponent},
	] },
	{ path: '', component: GullLayoutComponent, children: [
	//{ path: '', component: MatLayoutComponent, children: [
		{ path: 'administrator_dashboard', component: AdministratorDashboardComponent, canActivate: [AuthGuard]},
		{ path: 'user_dashboard', component: UserDashboardComponent, canActivate: [AuthGuard]},
		{ path: 'product_editor', component: ProductEditorComponent, canActivate: [AuthGuard]},
		{ path: 'list_of_products', component: ListOfProductsComponent, canActivate: [AuthGuard]},
		{ path: 'map', component: MapComponent, canActivate: [AuthGuard]},
		{ path: '**', redirectTo: '' },
	] },
];

export const routing = RouterModule.forRoot(appRoutes);
