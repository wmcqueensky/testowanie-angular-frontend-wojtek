import { LoaderService } from './../../services/loader.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BasicService } from 'src/app/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertsService } from 'src/app/services/alert.service';
import { ValidatorService } from 'src/app/bye/validator.service';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-list-of-products',
  templateUrl: './list-of-products.component.html',
  styleUrls: ['./list-of-products.component.scss'],
})

export class ListOfProductsComponent implements OnInit, OnDestroy {

  mapIsOpen: boolean= false;
  public loading: boolean = false;
  public title: string = 'LIST_OF_PRODUCTS_TITLE_TEXT'
  // The main and auxiliary model of the main part of the panel

  public model: any = {
    filter: {
      hint : ''
    },
    pagination: {
      mode: 'grid',
      start_page: 0,
      max_page_length: 12,
      asc: true,
    },
  };

  public list: any[] = [];
  public counter = 0;
  total_pages: number;
  view_mode = 'grid';
  public additional: any = {
  };

  constructor( private modalService: NgbModal, private alert: AlertsService, private translator: TranslateService,private activeRouter: ActivatedRoute, public basic: BasicService, public http: HttpClient, private router: Router, public navigation: NavigationService, private validator: ValidatorService, private loader: LoaderService) {
    const viewMode: string = window.innerWidth > 991 ? 'list' : 'grid';
    this.view_mode = viewMode;
    this.model.pagination.mode = viewMode;
  }

  ngOnInit(): void {
    this.navigation.setTitle(this.title);
    this.activeRouter.queryParamMap
    .subscribe( ( params: any ) => {
      try
      {
        const x = JSON.parse(params.params.query);
        if(x !== undefined)
        {
          let pagination = x['pagination'];
          if(pagination == undefined)
          {
            pagination = {};
            x['pagination'] = pagination;
          }

          pagination['max_page_length'] = 12;
          this.model = x;
        }
      }
      catch(exception)
      {
      }

      this.getPage();
    });
  }

  ngOnDestroy() {
    this.loading = false;
    this.loader.state = {
      showLoader: true
    }
  }

  getPage()
  {
    const req = 
    {
      metadata :
      {
        token : this.basic.getToken()
      },
      data : this.model
    };

    this.loading = true;

    this.loader.state = {
      showLoader: true
    }
    this.http.post(this.basic.getBaseUrl() + '/web/list_of_products/GetPage', req).subscribe(res => {
      this.loading = false;

      this.loader.state = {
        showLoader: false
      }
      try {
        const status = this.basic.processResponse(res);
        if (status === 'OK') {
          let max = this.model.pagination.max_page_length;
          if(max === undefined) max = 20;
          const l = res['data']['list'];
          if(l !== undefined)
          {
            if(l.length <= 0 && this.model.pagination.start_page > 0)
            {
              this.getPageWithNumber(this.model.pagination.start_page-1);
              return;
            }
            this.list = l;
          }

          const c = res['data']['counter'];
          if(c !== undefined) this.counter = c;
          if(this.counter % max == 0) this.total_pages = this.counter/max;
          else this.total_pages = Math.floor(this.counter/max) + 1;
          const a = res['data']['additional'];
          if (a !== undefined) this.additional = a;

        }
      } catch (exception) {
        this.router.navigate(['/login']);
      }
    }, err => {
      this.loading = false;
      this.loader.state = {
        showLoader: false
      }
    });
  }

  public getPageWithNumber(n: number)
  {
    this.model.pagination.start_page = n;
    this.getPage();
  }

  search(): void {
    delete this.model.pagination.max_page_length;
    this.model.pagination.start_page = 0;
    let x = JSON.stringify(this.model);

    this.router.navigate( ['/list_of_products'], {
      queryParams: { query: x},
      queryParamsHandling: 'merge',
    } );
  }

  public getQuery(page: number)
  {
    let x = JSON.parse(JSON.stringify(this.model));
    if(x['pagination'] === undefined) x['pagination'] = {};
    x['pagination']['max_page_length'] = undefined;
    x['pagination']['start_page'] = page;
    const y = JSON.stringify(x);
    return y;
  }

  sort(order_by: string,asc: boolean): void {
    this.model.pagination.asc = asc;
    this.model.pagination['order_by'] = order_by;
    this.search();
  }

  setViewMode(mode: string)
  {
    this.model.pagination.mode = mode;
    delete this.model.pagination.max_page_length;
    let x = JSON.stringify(this.model);

    this.router.navigate( ['/list_of_products'], {
      queryParams: { query: x},
      queryParamsHandling: 'merge',
    } );
  }

  public remove(question,recordId) {
    Swal.fire({
      title: this.translator.instant(question),
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.translator.instant('YES'),
      cancelButtonText: this.translator.instant('NO')
    } as any).then((result) => {
      if (result.value) {
        const req = {
          metadata : {
            token : this.basic.getToken()
          },
          data : {
            id : recordId
          }
        };

        this.loading = true;

        this.loader.state = {
          showLoader: true
        }
        this.http.post(this.basic.getBaseUrl() + '/web/list_of_products/RemoveDataWithId', req).subscribe(res => {
          this.loading = false;

          this.loader.state = {
            showLoader: false
          }
          try {
            const status = this.basic.processResponse(res);
            if (status === 'OK') {
              this.getPage();
            }
          } catch (exception) {
            this.router.navigate(['/login']);
          }
          console.log(JSON.stringify(res));
        }, err => {
          this.loading = false;
          this.loader.state = {
            showLoader: false
          }

        });
      }
    });
  }

  public gotoEditor(editor,recordId)
  {
    this.router.navigate( ['/' + editor], {
      queryParams: { id: recordId},
      queryParamsHandling: '',
    } );
  }

  open(modal: any, className: string = '') {
    this.modalService.open(modal, { size: 'lg', ariaLabelledBy: 'modal-basic-title', windowClass: className }).result.then(
      (result) => { },
      (reason) => { }
    );
  }
  public getDescription(key: string,value: any) : any {
    if(key == null || key === undefined || key === '' || value == null || value === undefined || this.additional == null || this.additional === undefined)
    {
      return value;
    }

    const list = this.additional[key];
    if(list == null || list === undefined)
    {
      return value;
    }

    const n = list.length;
    for(let i=0;i<n;i++)
    {
      const x = list[i];
      if(x == null || x === undefined) continue;
      if(x.value == value) return x.description;
    }

    return value;
  }

}