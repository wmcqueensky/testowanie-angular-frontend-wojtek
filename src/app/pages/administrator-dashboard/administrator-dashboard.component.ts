import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BasicService } from 'src/app/services/basic.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'administrator',
  templateUrl: './administrator-dashboard.component.html',
  styleUrls: ['./administrator-dashboard.component.scss']
})
export class AdministratorDashboardComponent implements OnInit, OnDestroy {
  public model: any = {};
  public title: string = 'ADMINISTRATOR_DASHBOARD_TITLE';
  position: { lat: number, lng: number } = { lat: 52.1724, lng: 20.8079 };

  constructor(
    private readonly navigationService: NavigationService,
    public basic: BasicService,
    private http: HttpClient,
    public translate: TranslateService,
    private cdref: ChangeDetectorRef,
    private router: Router,
    private loader: LoaderService
  ) { }


  ngOnInit(): void {
    this.navigationService.setTitle(this.title);
    const req = {
      metadata: {
        token: this.basic.getToken()
      },
      data: {
      }
    }


    this.loader.state = {
	      showLoader: true
    };
    this.http.post(this.basic.getBaseUrl() + '/web/administrator_dashboard/GetData', req).subscribe(res => {
    this.loader.state = {
	      showLoader: false
    };
      try {
        const status = this.basic.processResponse(res);
        if (status === 'OK') {
          this.model = res['data'];
        }
      } catch (exception) {
        this.router.navigate(['/login']);
      }
    }, err => {      this.loader.state = {
        showLoader: false
      }
    });

  }

  ngOnDestroy() {
    this.loader.state = {
      showLoader: true
    }
  }

  onPositionChanged(newPosition: { lat: number, lng: number }) {
    this.position = newPosition;
  }

}
