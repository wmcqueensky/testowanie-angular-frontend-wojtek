import { BehaviorSubject, Observable } from 'rxjs';


export interface IAction {
  action: string,
  payload?: any
}

export class GlobalActions {
  private static _state: IAction = {action: '__INIT__'};
  private static _state$ = new BehaviorSubject<IAction>(this._state);

  static get state(): IAction {
    return this._state;
  }

  static set state(state: IAction) {
    this._state = JSON.parse(JSON.stringify(state));
    this._state$.next(this._state);
  }

  static get state$(): Observable<IAction> {
    return this._state$.asObservable();
  }
}
