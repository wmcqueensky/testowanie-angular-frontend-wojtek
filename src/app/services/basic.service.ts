import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SocialAuthService } from 'angularx-social-login';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BasicService {
  private readonly PROD: boolean = environment.production;

  private loggedIn: boolean;
  private token?: string;
  private role?: string;
  private roles?: string[] = [];
  private login?: string;

  baseUrl: string = environment.baseUrl;

  constructor(
    private socialAuthService: SocialAuthService
  ) {}

  getBaseUrl() {
    return this.baseUrl;
  }

  processResponse(res: any): string {
    
    try {
      // tslint:disable-next-line: no-string-literal
      const x = res['metadata']['token'];
      if (x != null && x !== undefined && x !== '') {
        this.setToken(x);
      }
    } catch (exc) {
    }

    try {
      // tslint:disable-next-line: no-string-literal
      const x = res['metadata']['user_type'];
      if (x) {
        const arr = x.split('|');
        this.setUserRoles(arr);
      }
    } catch (exc) {
    }

    try {
      // tslint:disable-next-line: no-string-literal
      const x = res['metadata']['user_login'];
      if (x != null && x !== undefined && x !== '') {
        this.setLogin(x);
      }
    } catch (exc) {
    }

    try {
      // tslint:disable-next-line: no-string-literal
      const x = res['metadata']['status'];
      if (x != null && x !== undefined && x !== '') {
        return x;
      } else {
        return 'FAIL';
      }
    } catch (exc) {
    }
  }


  getToken(): string {
    if (!this.token) {
      let token: string = localStorage.getItem("token");
      if (token === 'null' || token === 'undefined') {
        token = '';
      }

      this.setToken(token);

      if (this.PROD) {
        localStorage.removeItem("token");
      }
    }

    return this.token;
  }

  private setToken(token: string) {
    this.token = token || '';
    if (this.PROD) return;
    localStorage.setItem('token', token);
  }

  public isLoggedIn() {
    if (!this.role) this.getUserRole();
    this.loggedIn = !!this.getToken();
    return this.loggedIn;
  }

  public getUserRole() {
    if (!this.role) {
      let role: string | 'null' | 'undefined' = localStorage.getItem("role") as string | 'null' | 'undefined';
      if (role === 'null' || role === 'undefined') {
        role = '';
      }

      this.setUserRole(role);

      if (this.PROD) {
        localStorage.removeItem("role");
      }
    }

    return this.role as string;
  }

  private setUserRole(role: string) {
    this.role = role || '';
    if (this.PROD) return;
    localStorage.setItem('role', role as string);
  }

  public getUserRoles() {
    if (!this.roles.length) {
      let roles: string | 'null' | 'undefined' = localStorage.getItem("roles") as string | 'null' | 'undefined';
      if (roles === 'null' || roles === 'undefined') {
        roles = '[]';
      }

      this.setUserRoles(JSON.parse(roles));

      if (this.PROD) {
        localStorage.removeItem("roles");
      }
    }

    return this.roles;
  }

  private setUserRoles(roles: string[]) {
    if (!roles || !roles.length || !Array.isArray(roles)) {
      this.roles = [];
    } else {
      this.roles = [...roles];
    }

    this.setUserRole(this.roles[0]);
    if (this.PROD) return;
    localStorage.setItem('roles', JSON.stringify(this.roles));
  }

  public getLogin(): string {
    if (!this.login) {
      let login: string = localStorage.getItem("login");
      if (login === 'null' || login === 'undefined') {
        login = '';
      }

      this.setLogin(login);

      if (this.PROD) {
        localStorage.removeItem("login");
      }
    }

    return this.login;
  }

  private setLogin(login: string) {
    this.login = login || '';
    if (this.PROD) return;
    localStorage.setItem('login', login);
  }

  public removeToken() : boolean {
    this.setToken('');
    return true;
  }

  logout(): Promise<any> {
    this.setToken('');
    this.setUserRoles([]);
    this.loggedIn = false;

    try {
      return this.socialAuthService.signOut();
    } catch (err) {
      console.error(err);
      return new Promise((_, rej) => {
        rej();
      });
    }
  }

  isAdmin(): boolean {
    return this.getUserRoles().includes('administrator');
  }

  isUser(): boolean {
    return this.getUserRoles().includes('user');
  }
}