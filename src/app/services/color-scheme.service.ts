import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';


export type CurrentColorScheme = 'light' | 'dark';
export type SelectedColorScheme = CurrentColorScheme | 'auto';

const SELECTED_COLOR_SCHEME = 'COLOR_SCHEME';


@Injectable({
  providedIn: 'root'
})
export class ColorSchemeService {
  private _selectedColorScheme?: SelectedColorScheme;
  private _selectedColorScheme$ = new BehaviorSubject<SelectedColorScheme|undefined>(this._selectedColorScheme);
  private _currentColorScheme?: CurrentColorScheme;
  private _currentColorScheme$ = new BehaviorSubject<CurrentColorScheme|undefined>(this._currentColorScheme);

  constructor() {}

  get selectedColorScheme(): SelectedColorScheme {
    if (!this._selectedColorScheme) {
      let selectedColorScheme: SelectedColorScheme | undefined = this.getSelectedColorScheme();
      selectedColorScheme = selectedColorScheme || 'auto';

      return selectedColorScheme;
    }

    return this._selectedColorScheme;
  }

  set selectedColorScheme(colorScheme: SelectedColorScheme) {
    this._selectedColorScheme = colorScheme;
    this.setSelectedColorScheme(this._selectedColorScheme);
    this.currentColorScheme = this._selectedColorScheme === 'auto' ? undefined : this._selectedColorScheme;
    this.currentColorScheme = this.currentColorScheme;
    this._selectedColorScheme$.next(this._selectedColorScheme);
  }

  get selectedColorScheme$(): Observable<SelectedColorScheme> {
    return this._selectedColorScheme$.asObservable();
  }

  get currentColorScheme(): CurrentColorScheme | undefined {
    if (!this._currentColorScheme) {
      const selectedColorScheme = this.selectedColorScheme;

      if (selectedColorScheme === 'dark' || selectedColorScheme === 'light') {
        this.currentColorScheme = selectedColorScheme;
      }
      if (selectedColorScheme === 'auto') {
        const mediaDark = window.matchMedia('(prefers-color-scheme: dark)');
        const mediaLight = window.matchMedia('(prefers-color-scheme: light)');
        
        if (mediaDark.matches) {
          this.currentColorScheme = 'dark';
        }
        if (mediaLight.matches) {
          this.currentColorScheme = 'light';
        }
      }
    }

    return this._currentColorScheme;
  }

  private set currentColorScheme(colorScheme: CurrentColorScheme) {
    this._currentColorScheme = colorScheme;
    this._currentColorScheme$.next(this._currentColorScheme);
    this.setupColorScheme();
  }

  get currentColorScheme$(): Observable<CurrentColorScheme> {
    return this._currentColorScheme$.asObservable();
  }

  init() {
    this.setupColorScheme();
    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', e => {
        const {media} = e.target as MediaQueryList;
        if (!media.includes('prefers-color-scheme')) return;
        
        const {matches} = window.matchMedia('(prefers-color-scheme: dark)');
        this.currentColorScheme = matches ? 'dark' : 'light';
      });
  }

  private setupColorScheme() {
    console.log('setupColorScheme')
    const colorScheme: CurrentColorScheme = this.currentColorScheme;

    switch (colorScheme) {
      case 'dark': 
        document.body.classList.remove('COLOR_CHEME_LIGHT');
        document.body.classList.add('COLOR_CHEME_DARK');
        break;

      default:
        document.body.classList.remove('COLOR_CHEME_DARK');
        document.body.classList.add('COLOR_CHEME_LIGHT');
    }
  }

  private getSelectedColorScheme(): SelectedColorScheme | undefined {
    let colorScheme: SelectedColorScheme | 'undefined' | 'null' | undefined | null = localStorage.getItem(SELECTED_COLOR_SCHEME) as any;
    if (colorScheme === 'null' || colorScheme === 'undefined') {
      colorScheme = undefined;
    }
    
    colorScheme = colorScheme || undefined;
    return colorScheme as SelectedColorScheme | undefined;
  }

  private setSelectedColorScheme(colorScheme: SelectedColorScheme) {
    localStorage.setItem(SELECTED_COLOR_SCHEME, colorScheme);
  }
}
