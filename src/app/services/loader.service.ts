import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';


interface IState {
  showLoader: boolean,
  loaderWrapClassName?: string,
  loaderClassName?: string,
}


@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private _state: IState = {
    showLoader: true
  }
  
  private _state$ = new BehaviorSubject<IState>(this._state);

  constructor() {}

  set state(state: IState) {
    this._state = state;
    this._state$.next(this._state);
  }

  get state(): IState {
    return this._state;
  }

  get state$(): Observable<IState> {
    return this._state$.asObservable();
  }
}
