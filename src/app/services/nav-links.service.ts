import { BehaviorSubject, Observable } from 'rxjs';
import { BasicService } from './basic.service';
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';


interface IItem {
  roleRegExp?: string | RegExp,
  title: string,
}

interface ISubNavLink extends IItem {
  linkTo: string | (number|string)[],
  title: string,
  iconClass?: string,
}

interface INavLink extends IItem {
  linkTo?: string | (number|string)[],
  title: string,
  iconClass: string,
  sub?: ISubNavLink[],
  showSubsStrategy?: 'show_only' | 'show_more_then_1' // default 'show_only'
}

export interface ISubNavItem {
  linkTo: string[],
  title: string,
  iconClass?: string,
  active: boolean,
}

export interface INavItem {
  title: string,
  linkTo?: string[],
  iconClass: string,
  sub: ISubNavItem[],
  active: boolean,
}


@Injectable({
  providedIn: 'root'
})
export class NavLinksService {
  private _links: INavLink[] = [
		{
			title : 'MENU_HEADER_PRODUCTS',
			iconClass : 'i-Library',
			roleRegExp : '^(administrator)$',
      linkTo : 'list_of_products',
		},
    {
			title : 'map',
			iconClass : 'i-Map-Marker',
			roleRegExp : '^(administrator)$',
      linkTo : 'map',
		},
	];

  private _links$ = new BehaviorSubject<INavItem[]>(this.links);

  constructor(
    private basic: BasicService,
    private router: Router
  ) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.links = this.links;
      }
  });
  }

  get links(): INavItem[] {
    const roles = this.basic.getUserRoles();
    
    const links =  this._links
      .filter(link => {
        if (!link.roleRegExp) return true;
        const reg = new RegExp(link.roleRegExp);
        return roles.some(role => reg.test(role));
      })
      .map(link => {
        link.showSubsStrategy = link.showSubsStrategy || 'show_only';
        const item: INavItem = {
          title: link.title,
          iconClass: link.iconClass,
          linkTo: this.formatLinkTo(link.linkTo),
          active: false,
          sub: (link.sub || [])
            .filter(s => {
              if (!s.roleRegExp) return true;
              const reg = new RegExp(s.roleRegExp);
              return roles.some(role => reg.test(role));
            })
            .map(s => {
              return {
                title: s.title,
                iconClass: s.iconClass,
                linkTo: this.formatLinkTo(s.linkTo),
                active: false,
              }
            })
        }

        if (link.showSubsStrategy === 'show_more_then_1' && item.sub.length === 1) {
          item.linkTo = item.sub[0].linkTo;
          item.sub = [];
        }

        return item;
      });
      
    return this.defineActive(links);
  }

  set links(items: INavItem[]) {
    this._links$.next(items);
  }

  get links$(): Observable<INavItem[]> {
    return this._links$.asObservable();
  }

  private formatLinkTo(links?: string | (number|string)[]): string[] {
    let href = links;
    if (!href) return;

    if (typeof(href) === 'object') {
      const firsItem = '' + href[0];
      if (firsItem[0] !== '/') href[0] = `/${firsItem}`;
    }

    if (typeof(href) === 'string') {
      const arr = href.split('/');
      if (!arr[0]) arr.shift();

      const firsItem = '' + arr[0];
      if (firsItem[0] !== '/') arr[0] = `/${firsItem}`;

      href = arr;
    }

    links = href.map(l => `${l}`);
    return links as string[];
  }

  private defineActive(links: INavItem[]): INavItem[] {
    let currentItem: INavItem;
    let currentSubItem: ISubNavItem;
    const pathname = location.pathname;

    for (let i = 0, l1 = links.length; i < l1; i++) {
      if (currentSubItem) break;

      const link = links[i];

      if (link.linkTo?.length) {
        let linkPathname = link.linkTo.join('/');
        linkPathname = linkPathname[0] === '/' ? linkPathname : '/' + linkPathname;
        if (pathname.includes(linkPathname)) {
          currentItem = link;
        }
      }

      for (let j = 0, l2 = link.sub.length; j < l2; j++) {
        const s = link.sub[j];
        let subLinkPathname = s.linkTo.join('/');
        subLinkPathname = subLinkPathname[0] === '/' ? subLinkPathname : '/' + subLinkPathname;
        if (pathname.includes(subLinkPathname)) {
          currentSubItem = s;
          currentItem = link;
          break;
        }
      }
    }
    
    if (currentItem) currentItem.active = true;
    if (currentSubItem) currentSubItem.active = true;
    return links || [];
  }
}
