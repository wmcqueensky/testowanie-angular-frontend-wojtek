import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

interface StrObj {
  [key: string]: string
}

interface ICookieConfig {
  path?: string,
  domain?: string,
  expires?: Date
}

@Injectable({
  providedIn: 'root'
})
export class CookieService {
  private forbiddenMarks: string[] = ['/', ';', '='];
  private _all: StrObj = {}
  private declare _all$: Subject<StrObj>;

  constructor() {
    this._all = this.getAll();
    this._all$ = new BehaviorSubject<StrObj>(this._all);
  }

  get all$(): Observable<StrObj> {
    return this._all$.asObservable();
  }

  getCookies(): string {
    return document.cookie;
  }

  getAll(): StrObj {
    const cookie: string = this.getCookies().trim();
    if (!cookie) return {};

    const cookieArr: string[] = cookie.split(';').map(c => c.trim());
    let cookieObj: StrObj = {}

    cookieArr.forEach(c => {
      const cArr = c.split('=').map(c => c.trim());
      cookieObj[cArr[0]] = cArr[1];
    });

    return cookieObj;
  }

  getItem(name: string): string | undefined {
    return this.getAll()[name];
  }

  setItem(name: string, value: string, config?: ICookieConfig): void {
    this._setItem(name, value, config);
    this.next();
  }

  removeItem(name: string): void {
    this._removeItem(name);
    this.next();
  }

  clear(): void {
    const all: StrObj = this.getAll();

    for (let k in all) {
      this._removeItem(k);
    }

    this.next();
  }

  private _setItem(name: string, value: string, config?: ICookieConfig): void {
    if (!config) config = {}
    const hasForbiddenMarks: boolean = this.forbiddenMarks.some(mark => {
      return name.includes(mark) || value.includes(mark);
    });

    if (hasForbiddenMarks) throw new Error(JSON.stringify(this.forbiddenMarks) + ' is forbidden marks');

    let str: string = `${name}=${value};`;

    if (config.path) {
      str += ` path=${config.path}`;
    }

    if (config.domain) {
      str += ` domain=${config.domain}`;
    }

    if (config.expires) {
      str += ` expires=${config.expires.toUTCString()}`;
    }

    document.cookie = str;
  }

  private _removeItem(name: string) {
    this.setItem(name, '', {expires: new Date(Date.now() - 1)});
  }

  private next() {
    this._all = this.getAll();
    this._all$.next(this._all);
  }
}
