import { Injectable, Inject } from '@angular/core';
import Swal, { SweetAlertResult } from 'sweetalert2';
import { resolve } from 'url';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(private translator: TranslateService) { }

  public showSuccess(title, text) {

    // tslint:disable-next-line: variable-name
    let translated_title: string;
    // tslint:disable-next-line: variable-name
    let translated_body: string;

    try {
      if (title !== undefined && title.length > 0) { translated_title = this.translator.instant(title); }
    } catch (error) {
      translated_title = '';
    }

    try {
      translated_body = this.translator.instant(text);
    } catch (error) {
      translated_body = '';
    }


    return Swal.fire(
      translated_title,
      translated_body,
      'success'
    );
  }

  public showError(title, text) {
    // tslint:disable-next-line: variable-name
    let translated_title: string;
    // tslint:disable-next-line: variable-name
    let translated_body: string;

    try {
      if (title !== undefined && title.length > 0) { translated_title = this.translator.instant(title); }
    } catch (error) {
      translated_title = '';
    }

    try {
      translated_body = this.translator.instant(text);
    } catch (error) {
      translated_body = '';
    }


    return Swal.fire(
      translated_title,
      translated_body,
      'error'
    );
  }

  public showInfo(title, text) {
    // tslint:disable-next-line: variable-name
    let translated_title: string;
    // tslint:disable-next-line: variable-name
    let translated_body: string;

    try {
      if (title !== undefined && title.length > 0) { translated_title = this.translator.instant(title); }
    } catch (error) {
      translated_title = '';
    }

    try {
      translated_body = this.translator.instant(text);
    } catch (error) {
      translated_body = '';
    }


    return Swal.fire(
      translated_title,
      translated_body,
      'info'
    );
  }


  public showTimeout(text) {
    // Przekierowuje, bo nie działa
    this.success(text);
    return;

    (Swal.mixin({
      toast: true,
      showConfirmButton: false,
      timer: 2000,
      type: 'success',
    } as any) as any).queue([
      this.translator.instant(text)
    ]);

  }


  public showLoading(text) {
    const t = this.translator.instant(text);

    Swal.fire({
      type: 'info',
      text: t,
      showConfirmButton: false,
// tslint:disable-next-line: max-line-length
      footer: '<div class="swal2-actions swal2-loading" style="display: flex;"><button type="button" class="swal2-confirm swal2-styled" style="display: flex; border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);" aria-label="" disabled="">OK</button><button type="button" class="swal2-cancel swal2-styled" style="display: none;" aria-label="" disabled="">Cancel</button></div>'
    } as any);
  }

  success(title: string, config: any = {}) {
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: this.translator.instant(title),
      showConfirmButton: false,
      timer: 1500,
      ...config
    });
  }

  confirm(header: string = '', text: string = '', config: any = {}): Promise<boolean> {
    return Swal.fire({
      title: header,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: config.confirmButtonText || 'Tak',
      cancelButtonText: config.cancelButtonText || 'Nie',
    }).then((result) => {
      if (result.isConfirmed) {
        return true;
      }
      return false;
    });
  }

  prompt(header?: string, text?: string, confirmButtonText?: string): Promise<string> {
    return Swal.fire({
      title: header ? header : '',
      text: text ? text : '',
      input: 'text',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmButtonText ? confirmButtonText : 'OK'
    }).then((result) => {
      if (result.isConfirmed) {
        return result.value || '';
      }
      return null;
    });
  }
}
