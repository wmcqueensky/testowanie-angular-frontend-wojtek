import { HostListener, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ILangs, Lang, LangService } from 'src/app/services/lang.service';

@Component({
  selector: 'app-lang-select',
  templateUrl: './lang-select.component.html',
  styleUrls: ['./lang-select.component.scss']
})
export class LangSelectComponent implements OnInit, OnDestroy {
  langDropdownOpen: boolean = false; // Czy dropdown dla wyboru języka otwarty
  langs: ILangs = this.langService.getLangs();
  selectedLang: Lang = this.langService.getCurrentLang();
  sub$!: Subscription;

  constructor(
    private langService: LangService
  ) {}

  ngOnInit(): void {
    this.sub$ = this.langService.getLang$()
      .subscribe({
        next: (lang: Lang) => {
          this.selectedLang = lang;
        },
        error: err => {
          console.error(err);
        }
      });
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(e: MouseEvent) {
    let path: HTMLElement[] = (e as any)?.path?.length || [];
    if ((e as any)?.path) {
      path = (e as any)?.path;
    } else if (e.composedPath) {
      path = e.composedPath() as HTMLElement[];
    }
    
    if (!path || path.length === 0) return;
    const dropdown = path.find(x => x?.id === 'lang_select_dropdown');
    const btn = path.find(x => x?.id === 'lang_select_btn');
    if (dropdown || btn) return;
    this.langDropdownOpen = false;
  }

  setLanguage(code: Lang): void {
    this.langService.setLang(code);
    this.langDropdownOpen = false;
  }

  ngOnDestroy(): void {
    try {
      this.sub$?.unsubscribe();
    } catch (_) {}
  }
}
