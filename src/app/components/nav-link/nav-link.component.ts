import { NavigationService, INavLink, ISubNavLink } from 'src/app/services/navigation.service';
import { Input, Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BasicService } from 'src/app/services/basic.service';


@Component({
  selector: 'app-nav-link',
  templateUrl: './nav-link.component.html',
  styleUrls: ['./nav-link.component.scss']
})
export class NavLinkComponent implements OnInit {
  @Input() data: INavLink
  @Input() selectedKey: string = null;

  @Output() selectedKeyChange: EventEmitter<string> = new EventEmitter<string>();


  readonly defaultIconClass: string = 'i-Library';

  constructor(
    private readonly navigationService: NavigationService,
    private translate: TranslateService,
    private basic: BasicService
  ) {}

  ngOnInit(): void {
    this.data.linkTo = this.data.linkTo ? this.formatLinkTo(this.data.linkTo) : undefined;
  }

  formatLinkTo(links: string | any[]): string[] {
    let href = links;

    if (!href) return;

    if (typeof(href) === 'object') {
      const firsItem = '' + href[0];
      if (firsItem[0] !== '/') href[0] = `/${firsItem}`;
    }

    if (typeof(href) === 'string') {
      const arr = href.split('/');
      
      if (!arr[0]) arr.shift();

      const firsItem = '' + arr[0];
      
      if (firsItem[0] !== '/') arr[0] = `/${firsItem}`;

      href = arr;
    }

    links = href.map(l => `${l}`);
    return links;
  }

  pushSubLinks() {
    let sub: ISubNavLink[] = this.data.sub;
    const role = this.basic.getUserRole();

    if (sub && typeof(sub) === 'object' && Array.isArray(sub) && sub.length > 0) {
      let buf = [];
      const n = sub.length;
      let i: number;
      for (i = 0; i < n; i++) {
        let s = sub[i];
        const roles = s['roleRegExp'];

        if (roles) {
          try {
            const reg = new RegExp(roles);
            if (!reg.test(role)) continue;
          } catch(e) {}
        }

        s.linkTo = this.formatLinkTo(s.linkTo);
        s.iconClass = s.iconClass || this.defaultIconClass;
        buf.push(s);
      }

      sub = buf;
    }
    else {
      sub = [];
    }

    this.navigationService.setSubLinks(sub || []);
  }

  changeSelectedKey(key: string) {
    this.selectedKey = key;
    this.selectedKeyChange.emit(this.selectedKey);
  }
}
