import { AlertsService } from '../../services/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { BasicService } from 'src/app/services/basic.service';
import { AuthService, IAuthResponse } from '../auth.service';
import { SocialAuthService } from 'angularx-social-login';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  helper = {};
  model: any = {
		login: '',
		pass: '',
		type: 'buyer'
	};

	loading = false;
  sub$: Subscription;
  declare form: FormGroup;

  constructor(
    private router: Router,
    private socialAuthService: SocialAuthService,
    public readonly basic: BasicService, 
    private readonly auth: AuthService,
    private readonly alert: AlertsService,
    private loaderService: LoaderService
  ) {}

  ngOnInit() {
    this.registerForm();
    this.sub$ = this.socialAuthService.authState
      .subscribe((user) => {
        if (!user?.authToken) return;
        this.loginSocial(user.authToken);
      });

    this.loaderService.state = {
      showLoader: false
    }
  }

  registerForm() {
    this.form = new FormGroup({
      login: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]),
      pass: new FormControl('', [
        Validators.required
      ]),
      consent_all: new FormControl(false, []),
      consent_to_regulations: new FormControl(false, [Validators.requiredTrue]),
      consent_to_email_marketing: new FormControl(false, [])
    });
  }

  register() {
    if (this.form.invalid) return;

    this.loaderService.state = {
      showLoader: true
    }
    
    this.loading = true;
    this.auth.register(this.model)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }

        const s = resp.status;
        
        if (s === 'REGULATIONS_NOT_ACCEPTED') {
          this.alert.showError(s, '');
          return;
        }
        if (s === 'SOME_VALUES_WERE_NOT_UNIQUE') {
          this.alert.showError(s, '');
          return;
        }

        if (resp.ok) {
          this.redirectAfterLogin(s);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      });
	}

  loginSocial(token: string) {
    this.loading = true;
    this.auth.loginSocial(token)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      })
  }

  loginWithFB() {
    this.auth.loginWithFB();
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  private redirectAfterLogin(status: string) {
    const role = this.basic.getUserRole();
    this.model.pass = '';

    if (status === 'OK') {
      this.router.navigate(['/profile']);
    } else if (status === 'UNFINISHED_REGISTRATION') {
      this.router.navigate(['/unfinished_registration']);
    } else if (status === 'UNVERIFIED_ACCOUNT') {
      this.alert.showInfo('Konto jeszcze nie zostało akceptowane', '');
    }
  }

  consentAllAreChecked(): boolean {
    return this.model.consent_to_regulations && this.model.consent_to_email_marketing;
  }

  changeConsent(e: InputEvent) {
    const checked: boolean = (<HTMLInputElement>e.target).checked;
    this.model.consent_to_regulations = checked;
    this.model.consent_to_email_marketing = checked;
  }

  ngOnDestroy() {
    if (this.sub$) {
      this.sub$.unsubscribe();
    }

    this.loaderService.state = {
      showLoader: false
    }
  }
}
