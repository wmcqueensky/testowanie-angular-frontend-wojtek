import { AlertsService } from '../services/alert.service';
import { BasicService } from '../services/basic.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';
import { ConsoleLogPipe } from '../pipes/console-log.pipe';

export interface IAuthResponse {
  ok: boolean,
  status: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private socialAuthService: SocialAuthService,
    private router: Router,
    private readonly basic: BasicService,
    private readonly alert: AlertsService
  ) {}

	hexToInt(a,b)
	{
    const x = a + '' + b;
    return parseInt(x, 16);
	}


	sha512ToIntArray(sha: String) : any[]
	{
		if(sha == null || sha == undefined ||  sha == '') return [];
		var i : number;
    var n = sha.length;
		
		var list = [];
		
		for(i=0;i<n-1;i++)
		{
			const a = sha.charAt(i);
			const b = sha.charAt(i+1);
			const x = this.hexToInt(a, b);
			if(x >= 0 && x <= 245) 
			{
				list.push(x);
			}
		}

    return list;
	}

	getRandomSequence(n: number) : String
	{
		var buf = '';
		var r : number;
		
		while(buf.length < n)
		{
			if(n-buf.length == 1)
			{
				r = Math.floor(9*Math.random());
				buf += r;
				break;
			}
			
			if(n-buf.length == 2)
			{
				r = Math.floor(99*Math.random());
				buf += r;
				break;
			}
			
			if(n-buf.length == 3)
			{
				r = Math.floor(100 + 27*Math.random());
				buf += r;
				break;
			}
						
			r = Math.floor(127*Math.random());
			buf += r;
			buf += ',';			
		}
		
		return buf;
	}

  getRandomIndexesList(x: String)
	{
		var tab = x.split(',');
		var list = [];
    var i,n = tab.length;

		for(i=0;i<n;i++)
		{
      const s = tab[i];
      list.push(+s);
		}

		return list;
	}

  intToHex(x : number) : String
	{
    var y = x.toString(16);
    if(y.length == 1) return '0' + y;
    else return y;
	}


	tangle(random: String,ints: any[]) : String
	{
		if(random == null || ints == null || random.length != ints.length) return null;
		var i : number;
    var n = random.length;
		var buf = '';
		for(i=0;i<n;i++)
		{
			const c : String = random.charAt(i);
			var a : number;
      switch(c)
      {
        case '0':
          a = 0;
          break;
        case '1':
          a = 1;
          break;
        case '2':
          a = 2;
          break;
        case '3':
          a = 3;
          break;
        case '4':
          a = 4;
          break;
        case '5':
          a = 5;
          break;
        case '6':
          a = 6;
          break;
        case '7':
          a = 7;
          break;
        case '8':
          a = 8;
          break;
        case '9':
          a = 9;
          break;
        case ',':
          a = 10;
          break;
      }
			const x = ints[i] + a;
			const h = this.intToHex(x);
			buf += h;
		}
		
		return buf;
	}


  getDataForTangledScenario(model: any) : any
  {
    const login = model.login;
    const pass = model.pass;
    const passHash : String = CryptoJS.SHA512(pass).toString(CryptoJS.enc.Hex);
    const s1 = CryptoJS.SHA512(login + '_' + passHash).toString(CryptoJS.enc.Hex)

    // Tworzenie losowego ciągu liczb

    var buf = '';
    while(buf.length < 128)
    {
      buf += Math.floor(Math.random()*1000);
      buf += ',';
    }

    // Tworzenie soli 

    const salt = CryptoJS.SHA512(buf).toString(CryptoJS.enc.Hex);

    // Tworzenie powtórnie zasolonego hasła użytkownika

    const s2 = CryptoJS.SHA512(s1 + '_' + salt).toString(CryptoJS.enc.Hex);

	  // Tworzenie tablicy indeksów, na których należy sprawdzać zgodność sekwencji próbnej z 
	  // hashem hasła na serwerze
		
    const i1 : any[] = this.sha512ToIntArray(s1);
		const random = this.getRandomSequence(i1.length);
		var i2 = this.getRandomIndexesList(random);
		const tangled = this.tangle(random, i1);


		// Tworzenie spisu znaków, które mają wystąpić na wylosowanych wcześniej miejscach w ponownie zasolonym
		// haśle użytkownika
		
		buf = '';
    var i : number;
    const n = i2.length;
		for(i=0;i<n;i++)
		{
      const x = s2.charAt(i2[i]);
			buf += x;
		}

    // Tworzenie wynikowego JSONa

    const res = {
      login : login,
      tangled : tangled,
      salt : salt,
      sequence : buf
    };

    console.log('JSON: ' + JSON.stringify(res));
		return res;
  }

  getDataForUnsafeScenario(model: any) : any
  {
    const req = {
      login : model.login,
      pass : model.pass
    };

    return req;
  }

  getDataForHashedScenario(model: any) : any
  {
    const login = model.login;
    const pass = model.pass;
    const passHash : String = CryptoJS.SHA512(pass).toString(CryptoJS.enc.Hex);
    const s1 = CryptoJS.SHA512(login + '_' + passHash).toString(CryptoJS.enc.Hex)

    const req =
    {
      login : login,
      pass : s1
    };

    return req;
  }


  login(model: any): Observable<IAuthResponse> {

    // Tutaj należy wybrać jeden z trzech typów logowania zgodny z procedurą logowania na serwerze

    // const data = this.getDataForTangledScenario(model); // Póki co najbezpieczniejsza procedura zakładająca wysyłanie na serwer podwójnie zasolonego hasła i danych niezwiązanych z hasłem umożliwiających na serwerze uiwerzytelnienie użytkownika
    const data = this.getDataForHashedScenario(model); // W tym scenariuszu na serwer wysyła się login i hash hasła
    // const data = this.getDataForUnsafeScenario(model); // W tym scenariuszu na serwer wysyła się login i hasło w postaci jawnej, więc należy go unikać poza ewentualny zastosowaniem podczas testów

    const req = {
      data : data
    };

    console.log(JSON.stringify(req, null, 2))
		return this.http.post(this.basic.getBaseUrl() + '/Login', req)
      .pipe(
        map((res: any) => {
          console.log('res: ' + JSON.stringify(res));

          const status = this.basic.processResponse(res);
          const x: boolean = status === 'OK' || status === 'UNVERIFIED_ACCOUNT' || status === 'UNFINISHED_REGISTRATION';

          return {
            ok: x,
            status
          }
        })
      );
	}

  register(model: any): Observable<IAuthResponse> {
		const userLogin = model.login.toLowerCase().trim();
		const pass = model.pass;

		const passHash = CryptoJS.SHA512(pass).toString(CryptoJS.enc.Hex);

		const req = {
			data: {
				login: userLogin,
				pass: passHash,
				consent_to_regulations: model.consent_to_regulations,
				consent_to_email_marketing: model.consent_to_email_marketing
			}
		};

		return this.http.post(this.basic.getBaseUrl() + '/Register', req)
      .pipe(
        map((res: any) => {
          const status = this.basic.processResponse(res);
          const x: boolean = status === 'OK' || status === 'UNVERIFIED_ACCOUNT' || status === 'UNFINISHED_REGISTRATION';

          return {
            ok: x,
            status
          }
        })
      );
	}

  reset(model: any): Observable<IAuthResponse> {
		const req = {
			data: {
				login: model.login
			}
		};

		return this.http.post(this.basic.getBaseUrl() + '/StartPassReset', req)
      .pipe(
        map((res: any) => {
          if (res['metadata']['status'] === 'OK') {
            this.alert.showInfo('', 'PASS_RESET_MESSAGE_OK');
          } else {
            this.alert.showInfo('', 'PASS_RESET_MESSAGE_FAIL');
          }

          return {
            ok: status === 'OK',
            status
          }
        })
      );
	}

  finishReset(model: {hash: string, pass: string}) {
    model.pass = CryptoJS.SHA512(model.pass).toString(CryptoJS.enc.Hex);
    const req = {
      metadata: {},
      data: model
    }

    return this.http.post(this.basic.getBaseUrl() + '/FinishPassReset', req)
      .pipe(
        map((res: any) => {
          return res?.metadata?.status === 'OK';
        })
      );
  }

  loginSocial(token: string): Observable<IAuthResponse> {
		const req = {
			metadata: {},
			data: {
				token
			}
		}
    
		return this.http.post(`${this.basic.getBaseUrl()}/SocialLogin`, req)
			.pipe(
        map((res: any) => {
          this.basic.processResponse(res);
          const status = res?.metadata?.status;
          const x: boolean = status === 'OK' || status === 'UNVERIFIED_ACCOUNT' || status === 'UNFINISHED_REGISTRATION';
          
          // try {
          //   if (!x) {
          //     throw new Error(status);
          //   }
          // } catch (err) {
          //   console.error(err);
          //   Swal.fire({
          //     icon: 'error',
          //     title: 'Error',
          //     text: err
          //   });
          // }

          return {
            ok: x,
            status
          }
        }, err => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: err
          });
        })
      );
	}

  loginWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  logout() {
    this.basic.logout()
      .then(_ => this.router.navigate(['/login']))
      .catch(err => {
        console.error(err);
        this.basic.logout();
        this.router.navigate(['/login']);
      });
  }
}
