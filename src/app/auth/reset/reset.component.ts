import { LoaderService } from './../../services/loader.service';
import { AlertsService } from '../../services/alert.service';
import { BasicService } from '../../services/basic.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService, IAuthResponse } from '../auth.service';
import { SocialAuthService } from 'angularx-social-login';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit, OnDestroy {
  model: any = {
		login: '',
		pass: '',
		type: 'buyer'
	};
	loading = false;
	returnUrl: string;

	helper = {

	};

  sub$: Subscription;

  constructor(
    private socialAuthService: SocialAuthService,
    private router: Router,
    private readonly auth: AuthService,
    public readonly basic: BasicService,
    private readonly alert: AlertsService,
    private loaderService: LoaderService
  ) {}

  ngOnInit() {
    this.sub$ = this.socialAuthService.authState
      .subscribe((user) => {
        if (!user?.authToken) return;

        this.loginSocial(user.authToken);
      });

    this.loaderService.state = {
      showLoader: false
    }
  }

  reset() {
    this.loading = true;
    this.loaderService.state = {
      showLoader: true
    }

		this.auth.reset(this.model)
      .subscribe((_) => {
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }
      }, err => {
        console.error(err);
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }
      });
	}

  loginSocial(token: string) {
    this.loading = true;
    this.auth.loginSocial(token)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      });
  }

  loginWithFB() {
    this.auth.loginWithFB();
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  private redirectAfterLogin(status: string) {
    const role = this.basic.getUserRole();
    this.model.pass = '';

    if (status === 'OK') {
      this.router.navigate(['/' + role + '_dashboard']);
    } else if (status === 'UNFINISHED_REGISTRATION') {
      this.router.navigate(['/unfinished_registration']);
    } else if (status === 'UNVERIFIED_ACCOUNT') {
      this.alert.showInfo('Konto jeszcze nie zostało akceptowane', '');
    }
  }

  ngOnDestroy() {
    if (this.sub$) {
      this.sub$.unsubscribe();
    }

    this.loaderService.state = {
      showLoader: true
    }
  }
}