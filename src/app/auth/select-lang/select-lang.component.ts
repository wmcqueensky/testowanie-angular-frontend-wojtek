import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ILangs, Lang, LangService } from '../../services/lang.service';

@Component({
  selector: 'app-select-lang',
  templateUrl: './select-lang.component.html',
  styleUrls: ['./select-lang.component.scss']
})
export class SelectLangComponent implements OnInit, OnDestroy {
  langs: ILangs = this.langService.getLangs();
  selectedLang: Lang = this.langService.getCurrentLang();
  sub$!: Subscription;

  constructor(
    public readonly langService: LangService
  ) {}

  ngOnInit(): void {
    this.sub$ = this.langService.getLang$().subscribe(
      (lang: Lang) => {
        this.selectedLang = lang;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  setLanguage(code: Lang): void {
    this.langService.setLang(code);
  }

  ngOnDestroy(): void {
    try {
      this.sub$?.unsubscribe();
    } catch (_) {}
  }
}
