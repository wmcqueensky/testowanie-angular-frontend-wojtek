import { AuthModalsService } from './../auth-modals.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SocialAuthService } from 'angularx-social-login';
import { AlertsService } from 'src/app/services/alert.service';
import { BasicService } from 'src/app/services/basic.service';
import { AuthService, IAuthResponse } from '../../auth.service';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.css']
})
export class LoginModalComponent implements OnInit {
  @ViewChild('loginModal') loginModal: HTMLElement;
  
  model: any = {
		login: '',
		pass: '',
		type: 'buyer'
	};
	loading = false;

  helper = {

	};

  modalRef: NgbModalRef;

  constructor(
    public modalService: NgbModal,
    private router: Router,
    private socialAuthService: SocialAuthService,
    public readonly basic: BasicService, 
    private readonly auth: AuthService,
    private readonly alert: AlertsService,
    private readonly authModals: AuthModalsService
  ) {}

  ngOnInit() {
    // this.socialAuthService.authState
    //   .subscribe((user) => {
    //     if (!user) return;
    //     this.loginSocial(user.authToken);
    //   });

    this.listen();
  }

  listen() {
    this.authModals.getAction()
      .subscribe(action => {
        switch (action) {
          case 'CLEAR': 
            this.modalService.dismissAll();
            break;
          case 'OPEN_LOGIN': 
            this.openModal();
            break;
        }
      });
  }

  clearModals() {
    this.authModals.setAction('CLEAR');
  }

  openRegisterModal() {
    this.clearModals();
    this.authModals.setAction('OPEN_REGISTER');
  }

  openModal() {
    this.modalRef = this.modalService.open(this.loginModal, { size: 'xl' });
  }

  closeModal() {
    try {
      this.modalRef.close();
    } catch (_) {}
  }

  loginToService() {
    this.loading = true;
    this.auth.login(this.model)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      });
	}

  loginSocial(token: string) {
    this.loading = true;
    this.auth.loginSocial(token)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      });
  }

  loginWithFB() {
    this.auth.loginWithFB();
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  private redirectAfterLogin(status: string) {
    this.closeModal();
    const role = this.basic.getUserRole();
    this.model.pass = '';

    if (status === 'OK') {
      this.router.navigate(['/' + role + '']);
    } else if (status === 'UNFINISHED_REGISTRATION') {
      this.router.navigate(['/unfinished_registration']);
    } else if (status === 'UNVERIFIED_ACCOUNT') {
      this.alert.showInfo('Konto jeszcze nie zostało akceptowane', '');
    }
  }
}