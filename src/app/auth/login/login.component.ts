import { LoaderService } from './../../services/loader.service';
import { AlertsService } from '../../services/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { BasicService } from '../../services/basic.service';
import { AuthService, IAuthResponse } from '../auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  model: any = {
		login: '',
		pass: '',
		type: 'buyer'
	};

	loading = false;
  helper = {}
  subs$: Subscription[] = [];
  returnUrl: {
    path: string,
    qp: {[k: string]: string}
  } = {
    path: '',
    qp: {}
  };

  constructor(
    private router: Router,
    private socialAuthService: SocialAuthService,
    public readonly basic: BasicService, 
    private readonly auth: AuthService,
    private readonly alert: AlertsService,
    private readonly route: ActivatedRoute,
    private loaderService: LoaderService
  ) {}

  ngOnInit() {
    this.subscribeQP();
    this.subscribeAuthState();
    this.loaderService.state = {
      showLoader: false
    }
  }

  subscribeQP() {
    const sub$ = this.route.queryParams
      .subscribe((qp: any) => {
        if (qp?.returnUrl) {
          const returnUrlStr = decodeURI(qp.returnUrl);
          let [path, other] = returnUrlStr.split('?');
          const qpObj = {}

          if (other) {
            other
              .split('&')
              .forEach(x => {
                const [k, ...v] = x.split('=');
                qpObj[k] = decodeURI(v.join(''));
              });
          }

          this.returnUrl = {
            path, 
            qp: qpObj
          }
        }
      });

    this.subs$.push(sub$);
  }

  subscribeAuthState() {
    const sub$ = this.socialAuthService.authState
      .subscribe((user) => {
        if (!user?.authToken) return;

        this.loginSocial(user.authToken);
      });

    this.subs$.push(sub$);
  }

  loginToService() {
    this.loading = true;
    this.loaderService.state = {
      showLoader: true
    }

    this.auth.login(this.model)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }

        this.loaderService.state = {
          showLoader: false
        }
      }, err => {
        console.error(err);
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }
      });
	}

  loginSocial(token: string) {
    this.loading = true;
    this.auth.loginSocial(token)
      .subscribe((resp: IAuthResponse) => {
        this.loading = false;

        if (resp.ok) {
          this.redirectAfterLogin(resp.status);
        }
      }, err => {
        console.error(err);
        this.loading = false;
      });
  }

  loginWithFB() {
    this.auth.loginWithFB();
  }

  loginWithGoogle() {
    this.auth.loginWithGoogle();
  }

  private redirectAfterLogin(status: string) {
    const role = this.basic.getUserRole();
    this.model.pass = '';

    if (status === 'OK') {
      if (this.returnUrl.path) {
        this.router.navigate([this.returnUrl.path], {
          queryParams: this.returnUrl.qp
        });
        return;
      }

      this.router.navigate(['/' + role + '_dashboard']);
    } else if (status === 'UNFINISHED_REGISTRATION') {
      this.router.navigate(['/unfinished_registration']);
    } else if (status === 'UNVERIFIED_ACCOUNT') {
      this.alert.showInfo('Konto jeszcze nie zostało akceptowane', '');
    }
  }

  ngOnDestroy() {
    this.subs$.forEach(sub$ => {
      try {
        sub$ && sub$.unsubscribe();
      } catch (_) {}
    });

    this.loaderService.state = {
      showLoader: true
    }
  }
}
