import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { BasicService } from 'src/app/services/basic.service';


export function roleGuard(roles: string[]) {

  @Injectable({
    providedIn: 'root'
  })
  class RoleGuard implements CanActivate {

    constructor(
      private router: Router, 
      private basic: BasicService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      const role = this.basic.getUserRole();
      if (roles.includes(role)) {
        return true;
      }

      this.router.navigate([`/${role}_dashboard`]);
      return false;
    }
  }

  return RoleGuard;
}
