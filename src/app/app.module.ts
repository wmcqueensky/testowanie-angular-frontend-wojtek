import { ListOfProductsComponent } from './pages/list-of-products/list-of-products.component';
import { ProductEditorComponent } from './pages/product-editor/product-editor.component';
import { UserDashboardComponent } from './pages/user-dashboard/user-dashboard.component';
import { AdministratorDashboardComponent } from './pages/administrator-dashboard/administrator-dashboard.component';
import { ByeeditorComponent } from './bye/byeeditor/byeeditor.component';
import { ByebuttonComponent } from './bye/byebutton/byebutton.component';
import { ResponseInterceptor } from "./interceptors/response.interceptor";
import { DateToStringPipe } from './pipes/date-to-string.pipe';
import { CutTextPipe } from './pipes/cut-text.pipe';
import { FilterImageFilesPipe } from './pipes/filter-image-files.pipe';
import { FilterOtherFilesPipe } from './pipes/filter-other-files.pipe';
import { ConsoleLogPipe } from './pipes/console-log.pipe';
import { PhoneDirective } from './directives/phone.directive';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgModule, Provider } from "@angular/core";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgImageSliderModule } from 'ng-image-slider';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ServiceWorkerModule } from '@angular/service-worker'; 
import { environment } from '../environments/environment';
import { NavLinkComponent } from './components/nav-link/nav-link.component';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AuthGuard } from './guards/auth.guard';
import { MatLayoutComponent } from './layouts/mat-layout/mat-layout.component';
import { GullLayoutComponent } from './layouts/gull-layout/gull-layout.component';
import { IconsModule } from './icons/icons.module';
import { AuthComponent } from './auth/auth.component';
import { FinishRegisterComponent } from './auth/finish-register/finish-register.component';
import { LoginComponent } from './auth/login/login.component';
import { ModalsComponent } from './auth/modals/modals.component';
import { RegisterComponent } from './auth/register/register.component';
import { ResetComponent } from './auth/reset/reset.component';
import { UnfinishedRegistrationComponent } from './auth/unfinished-registration/unfinished-registration.component';
import { LoginModalComponent } from './auth/modals/login-modal/login-modal.component';
import { RegisterModalComponent } from './auth/modals/register-modal/register-modal.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { ModelNumberDirective } from './bye/model-number.directive';
import { ByecheckboxComponent } from './bye/byecheckbox/byecheckbox.component';
import { ByedateComponent } from './bye/byedate/byedate.component';
import { ByedatetimeComponent } from './bye/byedatetime/byedatetime.component';
import { ByegalleryComponent } from './bye/byegallery/byegallery.component';
import { ByenumberComponent } from './bye/byenumber/byenumber.component';
import { ByepassComponent } from './bye/byepass/byepass.component';
import { ByeradioComponent } from './bye/byeradio/byeradio.component';
import { ByeselectComponent } from './bye/byeselect/byeselect.component';
import { ByeswitchComponent } from './bye/byeswitch/byeswitch.component';
import { ByetextComponent } from './bye/byetext/byetext.component';
import { ByetextareaComponent } from './bye/byetextarea/byetextarea.component';
import { ByeuploadComponent } from './bye/byeupload/byeupload.component';
import { ByefilelistComponent } from './bye/byefilelist/byefilelist.component';
import { ByemapComponent } from './bye/byemap/byemap.component';
import { ByetooltipComponent } from './bye/byetooltip/byetooltip.component';
import { NumbersonlyDirective } from './bye/numberfilter.directive';
import { ByesearchSelectComponent } from './bye/byesearch-select/byesearch-select.component';
import { ByepaginationComponent } from './bye/byepagination/byepagination.component';
import { NgxImageCompressService } from 'ngx-image-compress';
import { CookiesComponent } from './components/cookies/cookies.component';
import { ByeCalendarModule } from './bye/byecalendar/byecalendar.module';
import { ByepopoverComponent } from './bye/byepopover/byepopover.component';
import { SelectLangComponent } from './auth/select-lang/select-lang.component';
import { ObjectEntriesPipe } from './pipes/object-entries.pipe';
import { LangSelectComponent } from './components/lang-select/lang-select.component';
import { ColorSchemeSelectComponent } from './components/color-scheme-select/color-scheme-select.component';
import { MainLoaderComponent } from './components/main-loader/main-loader.component';
import { ByeimageuploaderComponent } from './bye/byeimageuploader/byeimageuploader.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { QuillModule } from 'ngx-quill';
import { FinishResetComponent } from './auth/finish-reset/finish-reset.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { MapComponent } from './components/map/map.component';

function interceptorFactory(interceptor: any): Provider {
  return {
    provide: HTTP_INTERCEPTORS,
    useClass: interceptor,
    multi: true
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideConfig() {}


@NgModule({
  imports: [
    ByeCalendarModule,
	HighchartsChartModule,
    NgbModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
	NgScrollbarModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    NgImageSliderModule,
	IconsModule,
    routing,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
	  defaultLanguage: 'pl',
      isolate: true
    }),
	  ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ImageCropperModule,
    QuillModule.forRoot()
  ],
  declarations: [
		ListOfProductsComponent,

		ProductEditorComponent,

		UserDashboardComponent,

		AdministratorDashboardComponent,

	MainLoaderComponent,
	LangSelectComponent,
	ColorSchemeSelectComponent,
	NavLinkComponent,
    AppComponent,
	MatLayoutComponent,
	GullLayoutComponent,
    AuthComponent,
    FinishRegisterComponent,
    LoginComponent,
    ModalsComponent,
    RegisterComponent,
    ResetComponent,
    UnfinishedRegistrationComponent,
    LoginModalComponent,
    RegisterModalComponent,
    PhoneDirective,
    NumbersonlyDirective,
	ByebuttonComponent,
	ByeswitchComponent,
    ByecheckboxComponent,
    ByedateComponent,
    ByedatetimeComponent,
    ByedatetimeComponent,
    ByefilelistComponent,
    ByenumberComponent,
    ByeradioComponent,
    ByeselectComponent,
	ByeswitchComponent,
    ByetextComponent,
    ByetextareaComponent,
    ByeuploadComponent,
    ByepassComponent,
    ByegalleryComponent,
	ByemapComponent,
    ByetooltipComponent,	
    ModelNumberDirective, 
    ByesearchSelectComponent,
    ByepaginationComponent,
    ByeimageuploaderComponent,
    ByeeditorComponent,
	CookiesComponent,
    CutTextPipe,
    DateToStringPipe,
    ConsoleLogPipe,
    FilterImageFilesPipe,
    FilterOtherFilesPipe,
    ByepopoverComponent,
	SelectLangComponent,
  ObjectEntriesPipe,
  FinishResetComponent,
  TruncatePipe,
  MapComponent
],
  providers: [
    NgxImageCompressService,
    AuthGuard,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '122058256859-7cpfhmquiorr11hf7g5rpfip0j6k7or3.apps.googleusercontent.com'
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider(
              '2801505750111850'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    },
	interceptorFactory(ResponseInterceptor)
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
