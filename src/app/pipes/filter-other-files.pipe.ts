import { Pipe, PipeTransform } from '@angular/core';
import { IMAGE_EXTENSIONS } from '../models/constants';

@Pipe({
  name: 'filterOtherFiles'
})
export class FilterOtherFilesPipe implements PipeTransform {

  transform(files: any[]): any[] {
    return files.filter(file => {
      return !IMAGE_EXTENSIONS.includes(file.extension);
    });
  }
}