import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectEntries'
})
export class ObjectEntriesPipe implements PipeTransform {

  transform(value: {[key: string]: any}): Array<[string, any]> {
    return Object.entries(value);
  }
}
