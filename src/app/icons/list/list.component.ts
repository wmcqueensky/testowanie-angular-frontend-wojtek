import { Component } from '@angular/core';

@Component({
  selector: 'icon-list',
  template: `
    <svg version="1.1" id="list" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 612 612" style="enable-background: new 0 0 612 612;" xml:space="preserve" fill="currentColor">
      <path d="M0,97.92v24.48h612V97.92H0z M0,318.24h612v-24.48H0V318.24z M0,514.08h612V489.6H0V514.08z" />
    </svg>
  `
})
export class IconListComponent {}